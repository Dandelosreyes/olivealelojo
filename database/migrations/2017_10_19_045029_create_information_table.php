<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lastname',50);
            $table->string('firstname',50);
            $table->string('middlename',50);
            $table->string('mailingaddress');
            $table->string('passport')->unique();
            $table->integer('gender')->default('1');
            $table->string('birthday',15);
            $table->string('civilstatus',30);
            $table->string('telnumber');
            $table->string('occupation');
            $table->string('destination');
            $table->string('placeofbirth');
            $table->string('history')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information');
    }
}
