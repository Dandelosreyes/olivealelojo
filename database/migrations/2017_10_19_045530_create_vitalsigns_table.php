<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVitalsignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vitalsigns', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('information_id')->unique();
            $table->foreign('information_id')->references('id')->on('information')->onDelete('cascade');
            $table->string('height',10);
            $table->string('weight',15);
            $table->string('bp',15);
            $table->string('pulse',15);
            $table->string('respiration',15);
            $table->string('bmi',15);
            $table->string('ppd',15);
            $table->string('lmp',15)->nullable();
            $table->string('lsc',15)->nullable();
            $table->string('method',15)->nullable();
            $table->string('mmr');
            $table->string('bodybuilt');
            $table->double('uncorrected_fv_od');
            $table->double('unccorected_fv_os');
            $table->double('corrected_fv_od');
            $table->double('corrected_fv_os'); 
            $table->double('uncorrected_nv_od');
            $table->double('unccorected_nv_os');
            $table->double('corrected_nv_od');
            $table->double('corrected_nv_os'); 
            $table->integer('optical');
            $table->integer('colorvision');
            $table->double('as');
            $table->double('ad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vitalsigns');
    }
}
