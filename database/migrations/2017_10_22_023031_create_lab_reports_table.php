<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lab_reports', function (Blueprint $table) {
            $table->increments('id');
          $table->unsignedInteger('information_id')->unique();
           $table->foreign('information_id')->references('id')->on('information')->onDelete('cascade');
           $table->string('xrayno')->nullable();
           $table->string('xrayexam')->nullable();
           $table->string('xrayfindings')->nullable();
           $table->string('cbc')->nullable();
           $table->string('cbc_hgb')->nullable();
           $table->string('bloodtype')->nullable();
           $table->string('urinalysis')->nullable();
           $table->string('directfecalsmear')->nullable();
           $table->string('vdrl')->nullable();
           $table->string('hepabtest')->nullable();
           $table->string('aidstest')->nullable();
           $table->string('pregnancytest')->nullable();
           $table->string('thyphoid')->nullable();
           $table->string('cholera')->nullable();
           $table->string('leprosy')->nullable();
           $table->string('alcoholtest')->nullable();
           $table->string('met')->nullable();
           $table->string('thc')->nullable();
           $table->string('ecg')->nullable()->nullable();
           $table->string('hepactest')->nullable();
           $table->string('tp_eia')->nullable();
           $table->string('malarialsmear')->nullable();
           $table->string('stoolculture')->nullable();
           $table->string('fbs_rbs')->nullable();
           $table->string('kidneyfunctest')->nullable();
           $table->string('kidneyfunctest_bun')->nullable();
           $table->string('kidneyfunctest_serum')->nullable();
           $table->string('liverfunctest')->nullable();
           $table->string('liverfunctest_sgot')->nullable();
           $table->string('liverfunctest_sgpt')->nullable();
           $table->string('liverfunctest_billirubin')->nullable();
           $table->string('liverfunctest_indirect')->nullable();
           $table->string('liverfunctest_direct')->nullable();
           $table->string('liverfunctest_alkaline')->nullable();
           $table->string('psychotest')->nullable();
           $table->string('remarks')->nullable();
           $table->string('suggestion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lab_reports');
    }
}
