<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('information_id')->unique();
            $table->foreign('information_id')->references('id')->on('information')->onDelete('cascade');
            $table->string('dentaltx');
             $table->integer('upper_right_8')->default('0');
            $table->integer('upper_right_7')->default('0');
            $table->integer('upper_right_6')->default('0');
            $table->integer('upper_right_5')->default('0');
            $table->integer('upper_right_4')->default('0');
            $table->integer('upper_right_3')->default('0');
            $table->integer('upper_right_2')->default('0');
            $table->integer('upper_right_1')->default('0');

            $table->integer('upper_left_8')->default('0');
            $table->integer('upper_left_7')->default('0');
            $table->integer('upper_left_6')->default('0');
            $table->integer('upper_left_5')->default('0');
            $table->integer('upper_left_4')->default('0');
            $table->integer('upper_left_3')->default('0');
            $table->integer('upper_left_2')->default('0');
            $table->integer('upper_left_1')->default('0');

            $table->integer('lower_right_8')->default('0');
            $table->integer('lower_right_7')->default('0');
            $table->integer('lower_right_6')->default('0');
            $table->integer('lower_right_5')->default('0');
            $table->integer('lower_right_4')->default('0');
            $table->integer('lower_right_3')->default('0');
            $table->integer('lower_right_2')->default('0');
            $table->integer('lower_right_1')->default('0');
            
            $table->integer('lower_left_8')->default('0');
            $table->integer('lower_left_7')->default('0');
            $table->integer('lower_left_6')->default('0');
            $table->integer('lower_left_5')->default('0');
            $table->integer('lower_left_4')->default('0');
            $table->integer('lower_left_3')->default('0');
            $table->integer('lower_left_2')->default('0');
            $table->integer('lower_left_1')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentals');
    }
}
