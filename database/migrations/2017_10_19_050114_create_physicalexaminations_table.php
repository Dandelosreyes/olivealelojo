<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalexaminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physicalexaminations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('information_id')->unique();
            $table->foreign('information_id')->references('id')->on('information')->onDelete('cascade');
            $table->string('skin');
            $table->string('neck');
            $table->string('pupils');
            $table->string('ears');
            $table->string('nose');
            $table->string('mouth');
            $table->string('thyroid'); 
            $table->string('chest');
            $table->string('lungs');
            $table->string('heart');
            $table->string('abdomen');
            $table->string('back');
            $table->string('anus');
            $table->string('gusystem');
            $table->string('inguinals');
            $table->string('reflexes');
            $table->string('extremities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physicalexaminations');
    }
}
