<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([ 'role' => 'Nurse' ]);
        Role::create([ 'role' => 'Lab Technician']);
        Role::create([ 'role' => 'Doctor' ]);
        Role::create([ 'role' => 'Administrator' ]);
    }
}
