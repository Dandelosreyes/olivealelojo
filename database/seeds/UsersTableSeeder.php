<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([ 'role_id' => 1, 'name' => 'Nurse', 'email' => 'nurse@example.com' , 'licensenumber' => '123', 'password' => '123']);
   		User::create([ 'role_id' => 2, 'name' => 'Lab Tech', 'email' => 'lt@example.com' , 'licensenumber' => '321', 'password' => '123']);
   		User::create([ 'role_id' => 3, 'name' => 'Doctor', 'email' => 'doctor@example.com' , 'licensenumber' => '1234', 'password' => '123']);
   		User::create([ 'role_id' => 4, 'name' => 'Administrator', 'email' => 'admin@example.com' , 'licensenumber' => '4321', 'password' => '123']);
    }
}
