<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Information::class, function (Faker $faker) {
	$civilstatus = ['Single','Married','Divorced','Widow'];
	$phoneNumber = ['+639176136853','+639176136233','+639176135553','+639176131234','+63917611234'];
	$destination = ['LOCAL','FOREIGN'];
    return [
        //
        'lastname' => $faker->lastName,
        'firstname' => $faker->firstName,
        'middlename' => $faker->lastName,
        'passport' => $faker->swiftBicNumber,
        'gender' => rand(0,1),
        'birthday' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'civilstatus' => $civilstatus[rand(0,3)],
        'telnumber' => $phoneNumber[rand(0,4)],
        'occupation' => $faker->jobTitle,
        'destination' => $destination[rand(0,1)],
        'placeofbirth' => $faker->address,
        'mailingaddress' => $faker->address
    ];
});
