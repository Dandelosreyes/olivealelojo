<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'login');

Auth::routes();

Route::get('patients','HomeController@patients')->middleware('doctor')->name('patients');
Route::get('labrep','HomeController@lab')->middleware('labtech')->name('labtech');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('print/{id}',"HomeController@print")->name('print');

Route::resource('users','UserController')->middleware('admin');

Route::resource('information','informationController')->except(['destroy'])->middleware('nurse');

Route::name('vitalsign.')->prefix('vitalsign/')->middleware('doctor')->group(function(){
	Route::get('create/{id}','VitalSignController@create')->name('create');
	Route::post('store/{id}','VitalSignController@store')->name('store');
	Route::get('edit/{id}','VitalSignController@edit')->name('edit');
	Route::patch('update/{id}','VitalSignController@update')->name('update');
});

Route::name('phyexam.')->prefix('phyexam/')->middleware('doctor')->group(function(){
	Route::get('create/{id}','PeController@create')->name('create');
	Route::post('store/{id}','PeController@store')->name('store');
	Route::get('edit/{id}','PeController@edit')->name('edit');
	Route::patch('update/{id}','PeController@update')->name('update');
});

Route::name('dental.')->prefix('dental/')->middleware('doctor')->group(function(){
	Route::get('create/{id}','DentalController@create')->name('create');
	Route::post('store/{id}','DentalController@store')->name('store');
	Route::get('edit/{id}','DentalController@edit')->name('edit');
	Route::patch('update/{id}','DentalController@update')->name('update');
});

Route::name('labreport.')->prefix('lab/')->middleware('labtech')->group(function(){
	Route::get('create/{id}','LabReportController@create')->name('create');
	Route::post('store/{id}','LabReportController@store')->name('store');
	Route::get('edit/{id}','LabReportController@edit')->name('edit');
	Route::patch('update/{id}','LabReportController@update')->name('update');
	Route::view('upload/{id}','lab/upload')->name('upload');
});
Route::name('feedback.')->prefix('final')->middleware('doctor')->group(function(){
	Route::get('create/{id}','FeedbackController@create')->name('create');
	Route::post('store/{id}','FeedbackController@store')->name('store');
	// Route::get('edit/{id}','DentalController@edit')->name('edit');
	// Route::patch('update/{id}','DentalController@update')->name('update');
});
Route::get('labresult/{id}','LabReportController@edit')->middleware('doctor')->name('labresult.view');
Route::patch('labresult/{id}','LabReportController@update')->middleware('doctor')->name('labresult.update');


	Route::get('upload','LabReportController@upload_view')->name('upload');
	Route::get('upload/{id}','LabReportController@select')->middleware('labtech')->name('upload.edit');
	Route::get('search/{id}/upload','LabReportController@viewImage')->name('upload.search');
	Route::post('upload/{id}/save','LabReportController@saveimage')->middleware('labtech')->name('upload.save');
Route::get('admin/patients','UserController@patients')->middleware('admin')->name('patients.admin');
Route::get('admin/patient/delete/{id}','UserController@deletePatient')->middleware('admin')->name('admin.delete');
Route::get('admin/logs','HomeController@logs')->middleware('admin')->name('logs');
Route::get('settings','HomeController@view_settings')->name('settings');
Route::PATCH('password/update/{id}','HomeController@password')->name('password.update');
Route::get('employee/update/{id}','HomeController@employee')->name('employee.password');
Route::PATCH('employee/reset/{id}','HomeController@password')->name('employee.password.update');
