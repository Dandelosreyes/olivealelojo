
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */
window.$ = window.jQuery = require('jquery');
window.Popper = require('popper.js').default;
window.axios = require('axios');
require('bootstrap');


require("datatables.net-bs4")();
