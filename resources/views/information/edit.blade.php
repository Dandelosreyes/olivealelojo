@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-primary mt-4 ">
	<div class="card-header">Update Patient {{ $patient->id }}</div>
	<div class="card-body">
		<form class="ml-3 mr-3"  method="POST" action="{{ route('information.update',['id' => $patient->id]) }}">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			@include('shared.alerts')
			<div class="row mt-3">
				<div class="form-group col-12 col-sm-4">
				    <label class="form-control-label" for="lastname">Lastname:</label>
				    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $patient->lastname }}" >
				</div>
				<div class="form-group col-12 col-sm-4">
					<label class="form-control-label" for="firstname">Firstname:</label>
				    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $patient->firstname }}"> 
				</div>
				<div class="form-group col-12 col-sm-4">
					<label class="form-control-label" for="middlename">Middlename:</label>
				    <input type="text" class="form-control" id="middlename" name="middlename" value="{{ $patient->middlename }}">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-12 col-sm-12">
					<label class="form-control-label" for="mailingaddress">Mailing Address:</label>
					<input type="text" class="form-control" id="mailingaddress" name="mailingaddress" value="{{ $patient->mailingaddress }}" >
				</div>
			</div>	
			<div class="row">
				<div class="form-group col-12 col-sm-12 col-lg-2">
				    <label class="form-control-label" for="passport">Passport:</label>
				    <input type="text" class="form-control" id="passport" name="passport" value="{{ $patient->passport }}">
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-2">
				    <label class="form-control-label" for="gender">Gender:</label>
				    <select id="gender" name="gender" class="form-control">
				    	<option value="1" @if($patient->gender == 1) selected @endif>Male</option>
				    	<option value="2" @if($patient->gender  == 2) selected @endif>Female</option>
				    </select>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-3">
				    <label class="form-control-label" for="birthday">Birthday:</label> 
				    <input type="date" class="form-control" id="birthday" name="birthday" value="{{ $patient->birthday }}" placeholder="dd/mm/yy">
				</div>
				<div class="form-group col-12 col-sm12 col-lg-5">
				    <label class="form-control-label" for="placeofbirth">Place of birth:</label> 
				    <input type="text" class="form-control" id="placeofbirth" name="placeofbirth" value="{{ $patient->placeofbirth }}">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-12 col-sm-12 col-lg-2">
					<label class="form-control-label" class="form-control">Civil Status:</label>
					<select id="civilstatus" name="civilstatus" class="form-control">
						<option value="1" @if($patient->civilstatus == "Single") selected @endif>Single</option>
						<option value="2" @if($patient->civilstatus == "Married") selected @endif>Married</option>
						<option value="3" @if($patient->civilstatus == "Divorced") selected @endif>Divorced</option>
						<option value="4" @if($patient->civilstatus == "Widow") selected @endif>Widow</option>
					</select>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-4">
					<label class="form-control-label" for="phonenum">Cellphone Number:</label> 
				    <input type="text" class="form-control" id="phonenum" name="phonenum" value="{{ $patient->telnumber }}" placeholder="">
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-3">
					<label class="form-control-label" for="occupation">Occupation:</label> 
				    <input type="text" class="form-control" id="occupation" name="occupation" value="{{ $patient->occupation }}" placeholder="HR Manager">
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-3">
					<label class="form-control-label" for="destination">Destination:</label> 
				    <select id="destination" name="destination" class="form-control">
						<option value="LOCAL" @if($patient->destination== "LOCAL") selected @endif>LOCAL</option>
						<option value="FOREIGN" @if($patient->destination == "FOREIGN") selected @endif>FOREIGN</option>
					</select>
					
				</div>
			</div>
			<hr>
			<div class="row">
				@foreach($diseases as $disease)
					<div class="col-12 col-sm-7 col-md-6">
						<input type="checkbox"  id="{{ $disease->id }}"  name="disease[{{$disease->id}}]" value="{{ $disease->id }}" {{ in_array($disease->id,explode(',',$patient->history)) ?'checked':'' }} >
						<label for="{{ $disease->id }}" class="control-label">{{ $disease->disease }}</label>
					</div>
				@endforeach
			</div>
			<hr>
			<div class="form-group row">
		      <div class="col-sm-10">
		        <button type="submit" class="btn btn-primary">Save</button>
		      </div>
		    </div>
		</form>
	</div>
</div>
</div>
@endsection