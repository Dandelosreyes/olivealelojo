@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 col-12 mx-auto">
			<div class="card border-dark mt-4">
			  <div class="card-header">Patient
			  	<a href="{{ route('information.index') }}" class="btn btn-primary btn-sm mr-auto float-right">
					Back
				</a>
				</div>
				<div class="card-body">
					@include('shared.alerts')
					<div class="row mt-3">
				<div class="form-group col-12 col-sm-4">
				    <label class="form-control-label" for="lastname">Lastname:</label>
				    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $patient->lastname }}" readonly>
				</div>
				<div class="form-group col-12 col-sm-4">
					<label class="form-control-label" for="firstname">Firstname:</label>
				    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $patient->firstname }}" readonly> 
				</div>
				<div class="form-group col-12 col-sm-4">
					<label class="form-control-label" for="middlename">Middlename:</label>
				    <input type="text" class="form-control" id="middlename" name="middlename" value="{{ $patient->middlename }}" readonly>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-12 col-sm-12">
					<label class="form-control-label" for="mailingaddress">Mailing Address:</label>
					<input type="text" class="form-control" id="mailingaddress" name="mailingaddress" value="{{ $patient->mailingaddress }}" readonly>
				</div>
			</div>	
			<div class="row">
				<div class="form-group col-12 col-sm-12 col-lg-2">
				    <label class="form-control-label" for="passport">Passport:</label>
				    <input type="text" class="form-control" id="passport" name="passport" value="{{ $patient->passport }}" readonly>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-2">
				    <label class="form-control-label" for="gender">Gender:</label>
				    <select id="gender" name="gender" readonly class="form-control">
				    	<option value="1" @if($patient->gender == 1) selected @endif>Male</option>
				    	<option value="2" @if($patient->gender == 2) selected @endif>Female</option>
				    </select>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-3">
				    <label class="form-control-label" for="birthday">Birthday:</label> 
				    <input type="date" class="form-control" id="birthday" name="birthday" value="{{ $patient->birthday }}" readonly>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-1">
				    <label class="form-control-label" for="birthday">Age:</label> 
				    <input type="text" class="form-control" id="birthday" name="birthday" value="{{ $patient->age }}" readonly>
				</div>
				<div class="form-group col-12 col-sm12 col-lg-4">
				    <label class="form-control-label" for="placeofbirth">Place of birth:</label> 
				    <input type="text" class="form-control" id="placeofbirth" name="placeofbirth" value="{{ $patient->placeofbirth }}" readonly>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-12 col-sm-12 col-lg-2">
					<label class="form-control-label" class="form-control">Civil Status:</label>
					 <input type="text" class="form-control"  value="{{ $patient->civilstatus }}" readonly>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-4">
					<label class="form-control-label" for="phonenum">Cellphone Number:</label> 
				    <input type="text" class="form-control" id="phonenum" name="phonenum" value="{{ $patient->telnumber }}" readonly>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-3">
					<label class="form-control-label" for="occupation">Occupation:</label> 
				    <input type="text" class="form-control" id="occupation" name="occupation" value="{{ $patient->occupation }}" readonly>
				</div>
				<div class="form-group col-12 col-sm-12 col-lg-3">
					<label class="form-control-label" for="destination">Destination:</label> 
				    <input type="text" class="form-control" id="destination" name="destination" value="{{ $patient->destination }}" readonly>
				</div>
			</div>
			<hr>
			<div class="row">
				@foreach($diseases as $disease)
					<div class="col-12 col-sm-7 col-md-6">
						<input disabled type="checkbox"  id="{{ $disease->id }}"  name="disease[{{$disease->id}}]" value="{{ $disease->id }}" {{ in_array($disease->id,explode(",",$patient->history)) ?'checked':'' }} >
						<label for="{{ $disease->id }}" class="control-label">{{ $disease->disease }}</label>
					</div>
				@endforeach
			</div>
			<div class="row">
				<a href="{{ route('information.edit',['id' => $patient->id]) }}" class="btn btn-info ml-3 mt-3 btn-sm">Edit</a>
			</div>
		</div>
	</div>
</div>
@endsection