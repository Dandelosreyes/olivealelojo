@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-4">
	  <div class="card-header">Patients
	  	<a href="{{ route('information.create') }}" class="btn btn-primary btn-sm mr-auto float-right">
			New Patient
		</a>
	</div>
	  <div class="card-body text-dark">
		<table class="table " id="myTable" >
		  <thead class="thead-inverse card-text">
		      <th>#</th>
		      <th>Name</th>
		      <th>Passport</th>
		      <th>Contact Number</th>
  		      <th>Action</th>
		      <th>Date Created</th>
		  </thead>
		  <tbody>
		    @include('shared.alerts')
		    @foreach($patients as $patient)
		    <tr>
		      	<td>{{ $patient->id }}</td>
		      	<td>{{ $patient->lastname }} {{ $patient->firstname }}, {{ $patient->middlename }}</td>
			    <td>{{ $patient->passport }}</td>	
			    <td>{{ $patient->telnumber }}</td>
			    <td>
					<a href="{{ route('information.show',['id' => $patient->id]) }}" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>
					<a href="{{ route('information.edit',['id' => $patient->id]) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> </a>
			    </td>
			    <td>{{ $patient->created_at->format('m-d-Y') }}</td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	  </div>
</div>
</div>
@endsection
