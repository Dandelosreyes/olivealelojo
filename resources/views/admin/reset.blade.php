@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-4 mx-auto">
			<div class="card border-dark mt-4">
			  <div class="card-header">Reset Password
			</div>
			  <div class="card-body text-dark">
			  	@include('shared.alerts')
			  	<form method="POST" action="{{ route('employee.password.update',['id' => $id] )}}">
			  		{{ method_field('PATCH')}}
			  		{{ csrf_field() }}
			  		<label class="form-control-label" for="password">Password:</label>
				    <input type="password" class="form-control" id="password" name="password">
				    <label class="form-control-label" for="password_confirmation">Confirm password:</label>
				    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
			  	<div class="mt-2">
				    <button class="btn btn-info btn-md">Save</button>
				    			  	</form>

			  	</div>
			  </div>
			</div>
		</div>
	</div>
</div>
@endsection