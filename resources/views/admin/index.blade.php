@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-4">
	  <div class="card-header">Patients
	</div>
	  <div class="card-body text-dark">
		<table class="table " id="myTable" >
		  <thead class="thead-inverse card-text">
		      <th>#</th>
		      <th>Name</th>
		      <th>Delete</th>
		      <th>Date Created</th>
		  </thead>
		  <tbody>
		    @include('shared.alerts')
		    @foreach($patients as $patient)
		    <tr>
		      	<td>{{ $patient->id }}</td>
		      	<td>{{ $patient->lastname }} {{ $patient->firstname }}, {{ $patient->middlename }}</td>
			    <td><a href="{{ route('admin.delete',['id' => $patient->id ]) }}" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i></td>
			    <td>{{ $patient->created_at->format('m-d-Y') }}</td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	  </div>
</div>
</div>
@endsection
