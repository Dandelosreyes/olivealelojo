@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-3">
		<div class="card-header">Update Physical Examination of Patient {{ $patient->id }}</div>
		<div class="card-body">
			@include('shared.alerts')
			<form method = "POST" action="{{ route('phyexam.update',['id' => $patient->id] ) }}">
				{{ csrf_field()}}
				{{ method_field('patch') }}
				<div class="row">
			      <div class="col-md-6 mb-2">
			        <div class="row">
			          <div class="col-md-5 mb-2">
			            <label for="skin">Skin</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="skin" name="skin" value="{{ $patient->skin }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="neck">Neck, Head, Scalp</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="neck" name="neck" value="{{ $patient->neck }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="pupils">Pupils, Optalmoscopic</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="pupils" name="pupils" value="{{ $patient->pupils }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="ears">Ears</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="ears" name="ears" value="{{ $patient->ears }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for='nose'>Nose, Sinuses</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="nose" name="nose" value="{{ $patient->nose }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="mouth">Mouth, Throat</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="mouth" name="mouth" value="{{ $patient->mouth }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="thyroid">Neck, L.N., Thyroid</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="thyroid" name="thyroid" value="{{ $patient->thyroid }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="chest">Chest, Breast, Axilla</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="chest" name="chest" value="{{ $patient->chest  }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="lungs">Lungs</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="lungs" name="lungs" value="{{ $patient->lungs  }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="heart">Heart</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="heart" name="heart" value="{{ $patient->heart }}">
			          </div><!--col-md-8-->
			        </div><!--row-->
			      </div><!--col-md-6-->
			      <div class="col-md-6 mb-2">
			        <div class="row">
			          <div class="col-md-5 mb-2">
			            <label for="abdomen">Abdomen</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="abdomen" name="abdomen" value="{{ $patient->abdomen }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="Back">Back</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="Back" name="back" value="{{ $patient->back }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="anus">Anus, Rectum</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="anus" name="anus" value="{{ $patient->anus }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="gusystem">G-U System</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="gusystem" name="gusystem" value="{{ $patient->gusystem }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="inguinals">Inguinals, Genitals</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="inguinals" name="inguinals" value="{{ $patient->inguinals }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="reflexes">Reflexes</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="reflexes" name="reflexes" value="{{ $patient->reflexes  }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="extremities">Extremities</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="extremities" name="extremities" value="{{ $patient->extremities  }}">
			          </div><!--col-md-8-->
			        </div><!--row-->
			      </div><!--col-md-6-->
			    </div><!--row-->
			  	<div class="row">
			  		<button class="btn btn-info ml-2">Save</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection