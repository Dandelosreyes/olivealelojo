@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-3">
		<div class="card-header">Physical Examination of Patient {{ $id }}</div>
		<div class="card-body">
			@include('shared.alerts')
			<form method = "POST" action="{{ route('phyexam.store',['id' => $id] ) }}">
				{{ csrf_field()}}
				<div class="row">
			      <div class="col-md-6 mb-2">
			        <div class="row">
			          <div class="col-md-5 mb-2">
			            <label for="skin">Skin</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="skin" name="skin" value="{{ old('skin') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="neck">Neck, Head, Scalp</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="neck" name="neck" value="{{ old('neck') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="pupils">Pupils, Optalmoscopic</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="pupils" name="pupils" value="{{ old('pupils') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="ears">Ears</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="ears" name="ears" value="{{ old('ears') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for='nose'>Nose, Sinuses</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="nose" name="nose" value="{{ old('nose') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="mouth">Mouth, Throat</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="mouth" name="mouth" value="{{ old('mouth') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="thyroid">Neck, L.N., Thyroid</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="thyroid" name="thyroid" value="{{ old('thyroid') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="chest">Chest, Breast, Axilla</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="chest" name="chest" value="{{ old('chest') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="lungs">Lungs</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="lungs" name="lungs" value="{{ old('lungs') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="heart">Heart</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="heart" name="heart" value="{{ old('heart') }}">
			          </div><!--col-md-8-->
			        </div><!--row-->
			      </div><!--col-md-6-->
			      <div class="col-md-6 mb-2">
			        <div class="row">
			          <div class="col-md-5 mb-2">
			            <label for="abdomen">Abdomen</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="abdomen" name="abdomen" value="{{ old('abdomen') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="Back">Back</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="Back" name="back" value="{{ old('back') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="anus">Anus, Rectum</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="anus" name="anus" value="{{ old('anus') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="gusystem">G-U System</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="gusystem" name="gusystem" value="{{ old('gusystem') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="inguinals">Inguinals, Genitals</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="inguinals" name="inguinals" value="{{ old('inguinals') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="reflexes">Reflexes</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="reflexes" name="reflexes" value="{{ old('reflexes') }}">
			          </div><!--col-md-8-->
			          <div class="col-md-5 mb-2">
			            <label for="extremities">Extremities</label>
			          </div><!--col-md-4-->
			          <div class="col-md-7 mb-2">
			            <input type="text" class="form-control" id="extremities" name="extremities" value="{{ old('extremities') }}">
			          </div><!--col-md-8-->
			        </div><!--row-->
			      </div><!--col-md-6-->
			    </div><!--row-->
			  	<div class="row">
			  		<button class="btn btn-info ml-2">Save</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection