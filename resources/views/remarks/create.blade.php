@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-3">
		<div class="card-header">Physical Examination of Patient {{ $id }}</div>
		<div class="card-body">
			@include('shared.alerts')
			<form method = "POST" action="{{ route('feedback.store',['id' => $id] ) }}">
				{{ csrf_field()}}
				<div class="row">
			        <div class="col-md-6 mb-2">
				        <div class="row">
				            <div class="col-md-5 mb-2">
				           		<label for="psychtest">Psycho Test</label>
				            </div><!--col-md-4-->
				            <div class="col-md-7 mb-2">
					            <select id="destination" name="psychtest" class="form-control">
									<option value="Adequately Adjusted personality" @if(old('psychtest') == "Adequately Adjusted personality") selected @endif>Adequately Adjusted personality</option>
									<option value="For further evaluation" @if("old('psychtest')" == "For further evaluation") selected @endif>For further evaluation</option>
									<option value="For further evaluation" @if("old('psychtest')" == "For further evaluation") selected @endif>For further evaluation</option>
								</select>
				            </div><!--col-md-8-->
				      	</div>
			 	 	</div>
			 	 	<div class="col-md-6 mb-2">
				        <div class="row">
				            <div class="col-md-5 mb-2">
				           		<label for="remarks">Remarks</label>
				            </div><!--col-md-4-->
				            <div class="col-md-7 mb-2">
					            <select id="remarks" name="remarks" class="form-control">
									<option value="FIT" @if(old('remarks') == "FIT") selected @endif>FIT</option>
									<option value="UNFIT" @if("old('remarks')" == "UNFIT") selected @endif>UNFIT</option>
									<option value="PENDING" @if("old('P')" == "PENDING") selected @endif>PENDING</option>
								</select>
				            </div><!--col-md-8-->
				      	</div>
			 	 	</div>
			 	 	<div class="col-md-12 mb-2">
				        <div class="row">
				            <div class="col-md-12 mb-2">
				           		<label for="remarks">Suggestions</label>
				            </div><!--col-md-4-->
				            <div class="col-md-12 mb-2">
					            <input type="text" class="form-control" name="suggestion">
				            </div><!--col-md-8-->
				      	</div>
			 	 	</div>
				</div>
				<div class="row">
			  		<button class="btn btn-info ml-2">Save</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection