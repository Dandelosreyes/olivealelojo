@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="card border-dark mt-4">
	  <div class="card-header">Patients
	</div>
	  <div class="card-body text-dark">
		<table class="table " id="myTable" >
		  <thead class="thead-inverse card-text">
		      <th>#</th>
		      <th>Name</th>
		      <th>Vital Sign</th>
		      <th>Dental Checkup</th>
		      <th>Physical Examination</th>
		      <th>Lab Result</th>
  		      <th>View</th>
		      <th>Date Created</th>
		  </thead>
		  <tbody>
		    @include('shared.alerts')
		    @foreach($patients as $patient)
		    <tr>
		      	<td>{{ $patient->id }}</td>
		      	<td>{{ $patient->lastname }} {{ $patient->firstname }}, {{ $patient->middlename }}</td>
			    <td>
			    	@if($patient->vitalsign->isEmpty())
			    	New <a href="{{ route('vitalsign.create',['id' => $patient->id]) }}">Record</a>
			    	@else
			    	<a href="{{ route('vitalsign.edit',['id' => $patient->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
			    	@endif
			    </td>
			    <td>
			    	@if($patient->dental->isEmpty())
			    	New <a href="{{ route('dental.create',['id' => $patient->id]) }}">Record</a>
			    	@else
			    	<a href="{{ route('dental.edit',['id' => $patient->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
			    	@endif
			    </td>
			    <td>
			    	@if($patient->phyexam->isEmpty())
			    	New <a href="{{ route('phyexam.create',['id' => $patient->id]) }}">Record</a>
			    	@else
			    	<a href="{{ route('phyexam.edit',['id' => $patient->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
			    	@endif
			    </td>
			    <td>
			    	@if($patient->lab->isEmpty())
			    	Pending.
			    	@else
			    	<a href="{{ route('feedback.create',['id' => $patient->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
			    	@endif
			    </td>
			    <td>
					@if($patient->lab->isEmpty() || $patient->phyexam->isEmpty() || $patient->dental->isEmpty() )
						Incomplete Test.
					@else
						<a href="{{ route('print',['id' => $patient->id]) }}" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>
					@endif
			    </td>
			    <td>{{ $patient->created_at->format('m-d-Y') }}</td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	  </div>
</div>
</div>
@endsection
