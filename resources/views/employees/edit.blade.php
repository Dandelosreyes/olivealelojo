@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-8 col-lg-8 col-xs-12 col-12 mx-auto">
			<div class="card border-dark mt-4">
			  <div class="card-header">Employees
			  	<a href="{{ route('users.index') }}" class="btn btn-primary btn-sm mr-auto float-right">
					Back
				</a>
				</div>
				<div class="card-body">
					@include('shared.alerts')
					<form method="POST" action="{{ route('users.update',['id' => $user->id]) }}">
						{{ method_field('PATCH') }}
						{{ csrf_field() }}
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12">
								<label for="name" class="form-control-label">Fullname:</label>
								<input type="text"  class="form-control" id="name" name="name" value="{{ $user->name }}">
							</div> 
							<div class="col-12 col-sm-12 col-md-12 col-lg-12">
								<label for="email" class="form-control-label">Emailaddress:</label>
								<input type="text"  class="form-control" id="email" name="email" value="{{ $user->email }}">
							</div> 
							<div class="col-12 col-sm-12 col-md-12 col-lg-12">
								<label for="role" class="form-control-label">Role:</label>
								<select  id="role" name="role" class="form-control">
									@foreach($roles as $role)
										<option value="{{ $role->id}}" @if($user->role_id == $role->id) selected @endif>{{ $role->role }}</option>
									@endforeach
								</select>
							</div> 
							<div class="col-12 col-sm-12 col-md-12 col-lg-12">
								<label for="licensenumber" class="form-control-label">License Number:</label>
								<input type="text"  class="form-control" id="licensenumber" name="licensenumber" value="{{ $user->licensenumber }}">
							</div> 
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 mt-3">
								<button href="{{ route('users.edit',['id' => $user->id]) }}" class="btn btn-info">Update</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection