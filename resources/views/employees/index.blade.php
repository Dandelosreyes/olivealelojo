@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-4">
	  <div class="card-header">Employees
	  	<a href="{{ route('users.create') }}" class="btn btn-primary btn-sm mr-auto float-right">
			New Employee
		</a>
	</div>
	  <div class="card-body text-dark">
		<table class="table " id="myTable" >
		  <thead class="thead-inverse card-text">
		      <th>#</th>
		      <th>Name</th>
		      <th>Email</th>
		      <th>Role</th>
  		      <th>Action</th>
		      <th>Date Created</th>
		  </thead>
		  <tbody>
		    @include('shared.alerts')
		    @foreach($users as $user)
		    <tr>
		      	<td>{{ $user->id }}</td>
		      	<td>{{ $user->name }}</td>
			    <td>{{ $user->email }}</td>	
			    <td>{{ $user->roles->role }}</td>
			    <td>
					<a href="{{ route('users.show',['id' => $user->id]) }}" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>
					<a href="{{ route('users.edit',['id' => $user->id]) }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> </a>
					<a href="{{ route('employee.password',['id'=> $user->id]) }}" class="btn btn-info btn-sm"><i class="fa fa-key" aria-hidden="true"></i>
					</a>
					<form method='POST' action ="{{ route('users.destroy',[ 'id' => $user->id ]) }}" style="display: inline-block;">
						{{ csrf_field() }} {{ method_field('DELETE') }} 
						<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
					</form>
			    </td>
			    <td>{{ $user->created_at->format('m-d-Y') }}</td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	  </div>
</div>
</div>
@endsection
