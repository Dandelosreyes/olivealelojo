@extends('layouts.app')
@section('content')
<div class ="container">
  <div class="card card-info mt-3">
    <div class="card-header">Lab Report Examination</div>
    <div class="card-body">
      @include('shared.alerts')
      <form method="POST"  action="@if(Auth::user()->role_id != 3){{ route('labreport.update',['id' => $patient->id ]) }} @else {{ route('labresult.update',['id' => $patient->id ]) }} @endif">
        {{ csrf_field() }}
        {{ method_field('patch') }}
       <div class="row ">
            <div class="col-md-12">
              <label>A. Chest X-Ray</label>
            </div>
            <div class="col-md-4 ">
              <label>X-Ray Examination</label>
              <select class="form-control" id="xrayexam" name="xrayexam">
                <option value="0" @if($patient->xrayexam == 0) selected @endif>PA</option>
                <option value="1" @if($patient->xrayexam == 1) selected @endif>Lordotic</option>
              </select>
            </div>
            <div class="col-md-4 ">
              <label for="">No.</label>      
              <input type="text"  class="form-control" name="xrayno" value="{{ $patient->xrayno }}">
            </div>
            <div class="col-md-4 ">
              <label for="">Significant Findings</label>
              <input type="text"  class="form-control" name="xrayfindings" value="{{ $patient->xrayfindings }}">
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12">
              <label for="">B. ECG Report</label>
            </div>
            <div class="col-md-2 ">
              <label for="">Result</label>
            </div>
            <div class="col-md-10 ">
            <input type="text" class="form-control" name="ecg" value="{{ $patient->ecg }}">
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12">
              <label>C. Phase I</label>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>1. Complete Blood Count</label>
            </div>
            <div class="col-md-3 ">
              <select class="form-control" id="cbc" name="cbc">
                <option value="not required" @if($patient->cbc == "not required") selected @endif>Not Required</option>
                <option value="normal" @if($patient->cbc == "normal") selected @endif>Normal</option>
                <option value="not normal" @if($patient->cbc == "not normal") selected @endif>Not Normal</option>
              </select>
            </div>
            <div class="col-md-1 ">
              <label>HGB</label>
            </div>
            <div class="col-md-3 ">
              <input type="text" class="form-control" name="cbc_hgb" value="{{ $patient->cbc_hgb }}">
            </div>
            <div class="col-md-4 ml-3 ">
              <label>2. Blood Type</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="bloodtype" name="bloodtype">
                <option value="A+">A+</option>
                <option value="0+">0+</option>
                <option value="AB+">AB+</option>
                <option value="A-">A-</option>
                <option value="O-">O-</option>
                <option value="B-">B-</option>
                <option value="AB-">AB-</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>3. Urinalysis</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="urinalysis" name="urinalysis">
                <option value="not required" @if($patient->urinalysis == "not required") selected @endif>Not Required</option>
                <option value="normal" @if($patient->urinalysis == "normal") selected @endif>Normal</option>
                <option value="not normal" @if($patient->urinalysis == "not normal") selected @endif>Not Normal</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>4. Direct Fecal Smear</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="fecal" name="fecal">
                <option value="not required" @if($patient->fecal == "not required") selected @endif>Not Required</option>
                <option value="normal" @if($patient->fecal == "normal") selected @endif>Normal</option>
                <option value="not normal" @if($patient->fecal == "not normal") selected @endif>Not Normal</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>5. VDRL</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="vdrl" name="vdrl">
                <option value="positive" @if($patient->vdrl == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->vdrl == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->vdrl == "not required") selected @endif>Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>6. Hepa B Test</label>
            </div>
            <div class="col-md-7 ">
               <select class="form-control" id="hepab" name="hepab">
                <option value="positive" @if($patient->hepabtest == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->hepabtest == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->hepabtest == "not required") selected @endif>Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>7. AIDS Test</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="aids" name="aids">
                <option value="positive" @if($patient->aidstest == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->aidstest == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->aidstest == "not required") selected @endif>Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>8. Pregnancy Test</label>
            </div>
            <div class="col-md-7 ">
             <select class="form-control" id="pt" name="pt">
                <option value="positive" @if($patient->pregnancytest == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->pregnancytest == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->pregnancytest == "not required") selected @endif>Not Required</option>
              </select>
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12">
              <label>D. Phase II</label>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>1. Hepa C Test</label>
            </div>
            <div class="col-md-7 ">
             <select class="form-control" id="hepac" name="hepac">
                <option value="positive" @if($patient->hepactest == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->hepactest == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->hepactest == "not required") selected @endif>Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>2. TP-EIA</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="tp" name="tp">
                <option value="positive" @if($patient->tp_eia == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->tp_eia == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->tp_eia == "not required") selected @endif>Not Required</option>
              </select>
            </div>
          
            <div class="col-md-4 ml-3 ">
              <label>3. Malarial Smear</label>
            </div>
            <div class="col-md-7 ">
            <select class="form-control" id="ms" name="ms">
                 <option value="positive" @if($patient->malarialsmear == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->malarialsmear == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->malarialsmear == "not required") selected @endif>Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>4. Stool Culture</label>
            </div>
            <div class="col-md-7">
              <input class="form-control" name="stool" value="{{ $patient->stoolculture }}">
            </div>
            <div class="col-md-12 ml-3 ">
              <label>5. Blood Chemistry</label>
            </div>
            <div class="col-md-4 ml-5 ">
              <label for="">A. FBS/ RBS</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="fbs" value="{{ $patient->fbs_rbs }}">
            </div>
            <div class="col-md-4 ml-5 ">
              <label for="">B. Kidney Function Test</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="kft" value="{{ $patient->kidneyfunctest }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">B1. BUN</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="kft_bun" value="{{ $patient->kidneyfunctest_bun }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">B2. Serum Creatinine</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="kft_serum" value="{{ $patient->kidneyfunctest_serum }}">
            </div>
            <div class="col-md-4 ml-5 ">
              <label for="">C. Liver Function Test</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft" value="{{ $patient->liverfunctest }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C1. SGOT</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_sgot" value="{{ $patient->liverfunctest_sgot }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C2. SGPT</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_sgpt" value="{{ $patient->liverfunctest_sgpt }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C3. Total Billrubin</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_total" value="{{ $patient->liverfunctest_billirubin }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C4. Indirect Billrubin (B1)</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_indirect" value="{{ $patient->liverfunctest_indirect }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C5. Direct Billrubin (B1)</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_direct" value="{{ $patient->liverfunctest_direct }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C6. Alkaline Phospharase</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_alkaline" value="{{ $patient->liverfunctest_alkaline }}">
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12 ">
              <label>E. Others</label>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>1. Typhoid</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="typhoid" name="typhoid">
                  <option value="positive" @if($patient->thyphoid == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->thyphoid == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->thyphoid == "not required") selected @endif>Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>2. Cholera</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="cholera" name="cholera">
                <option value="positive" @if($patient->cholera == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->cholera == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->cholera == "not required") selected @endif>Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>3. Leprosy</label>
            </div>
            <div class="col-md-7 ">
             <select class="form-control" id="leprosy" name="leprosy">
                 <option value="positive" @if($patient->leprosy == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->leprosy == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->leprosy == "not required") selected @endif>Not Required</option>
              </select>
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12 ">
              <label>F. Drug Test</label>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>1. Alcohol Test</label>
            </div>
            <div class="col-md-7 ">
               <select class="form-control" id="alcohol" name="alcohol">
                 <option value="positive" @if($patient->alcoholtest == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->alcoholtest == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->alcoholtest == "not required") selected @endif>Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>2. MET</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="met" name="met">
                <option value="positive" @if($patient->met == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->met == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->met == "not required") selected @endif>Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>3. THC</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="thc" name="thc">
                 <option value="positive" @if($patient->thc == "positive") selected @endif>Positive</option>
                <option value="negative" @if($patient->thc == "negative") selected @endif>Negative</option>
                <option value="not required" @if($patient->thc == "not required") selected @endif>Not Required</option>
              </select>
            </div>
          </div>
          @if(Auth::user()->role_id == 3)
           <div class="row mt-3">
            <div class="col-md-4 ml-3 ">
              <label>REMARKS</label>
            </div>
            <div class="col-md-7 mb-3">
               <select class="form-control" id="remarks" name="remarks">
                <option value="FIT" @if($patient->remarks == "FIT") selected @endif>FIT</option>
                <option value="UNFIT" @if($patient->remarks == "UNFIT") selected @endif>UNFIT</option>
                <option value="PENDING" @if($patient->remarks == "PENDING") selected @endif>PENDING</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>Psycho Test</label>
            </div>
            <div class="col-md-7 mb-3">
               <select class="form-control" id="psychotest" name="psychotest">
                <option value="Adequately adjusted personally" @if($patient->psychotest == "Adequately adjusted personally") selected @endif>Adequately adjusted personally</option>
                <option value="For further revaluaton" @if($patient->psychotest == "For further revaluaton") selected @endif>For further revaluaton</option>
                <option value="NR" @if($patient->psychotest == "NR") selected @endif>NR</option>
              </select>
            </div>
             <div class="col-md-4 ml-3 ">
              <label>Suggestion</label>
            </div>
            <div class="col-md-7 mb-3">
               <input type="text" class="form-control" name="suggestion" value="{{ $patient->suggestion }}">
            </div>
          </div>
          @endif
          <div class="col-md-12">
          <button  class="btn btn-info">Save</button>
      </div>
    </form>
  </div>
</div>
@endsection