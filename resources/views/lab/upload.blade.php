@extends('layouts.app')
@section('content')
<div class ="container">
	<div class="card card-info mt-3">
		<div class="card-header">Lab Report Examination</div>
		<div class="card-body">
			@include('shared.alerts')
			<form method="POST" action="{{ route('labreport.store',['id' => $id ]) }}">
				{{ csrf_field() }}

			 <div class="row ">
            <div class="col-md-12">
              <label>A. Chest X-Ray</label>
            </div>
            <div class="col-md-4 ">
             	<label>X-Ray Examination</label>
             	<select class="form-control" id="xrayexam" name="xrayexam">
             		<option value="CHEST PA" @if(old('xrayexam') == "CHEST PA") selected @endif>PA</option>
             		<option value="Lordotic" @if(old('xrayexam') == "Lordotic") selected @endif>Lordotic</option>
             	</select>
            </div>
      </div>
</form>
</div>
</div>
</div>
@endsection