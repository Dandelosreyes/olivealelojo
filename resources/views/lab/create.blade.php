@extends('layouts.app')
@section('content')
<div class ="container">
	<div class="card card-info mt-3">
		<div class="card-header">Lab Report Examination</div>
		<div class="card-body">
			@include('shared.alerts')
			<form method="POST" action="{{ route('labreport.store',['id' => $id ]) }}">
				{{ csrf_field() }}

			 <div class="row ">
            <div class="col-md-12">
              <label>A. Chest X-Ray</label>
            </div>
            <div class="col-md-4 ">
             	<label>X-Ray Examination</label>
             	<select class="form-control" id="xrayexam" name="xrayexam">
             		<option value="CHEST PA" @if(old('xrayexam') == "CHEST PA") selected @endif>PA</option>
             		<option value="Lordotic" @if(old('xrayexam') == "Lordotic") selected @endif>Lordotic</option>
             	</select>
            </div>
            <div class="col-md-4 ">
              <label for="">No.</label>      
              <input type="text"  class="form-control" name="xrayno" value="{{ old('xrayno') }}">
            </div>
            <div class="col-md-4 ">
              <label for="">Significant Findings</label>
              <input type="text"  class="form-control" name="xrayfindings" value="{{ old('xrayfindings') }}">
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12">
              <label for="">B. ECG Report</label>
            </div>
            <div class="col-md-2 ">
              <label for="">Result</label>
            </div>
            <div class="col-md-10 ">
            <input type="text" class="form-control" name="ecg" value="{{ old('ecg') }}">
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12">
              <label>C. Phase I</label>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>1. Complete Blood Count</label>
            </div>
            <div class="col-md-3 ">
              <select class="form-control" id="cbc" name="cbc">
              	<option value="not required">Not Required</option>
              	<option value="normal">Normal</option>
              	<option value="not normal">Not Normal</option>
              </select>
            </div>
            <div class="col-md-1 ">
              <label>HGB</label>
            </div>
            <div class="col-md-3 ">
              <input type="text" class="form-control" name="cbc_hgb" value="{{ old('cbc_hgb') }}">
            </div>
            <div class="col-md-4 ml-3 ">
              <label>2. Blood Type</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="bloodtype" name="bloodtype">
              	<option value="A+">A+</option>
              	<option value="0+">0+</option>
              	<option value="AB+">AB+</option>
              	<option value="A-">A-</option>
              	<option value="O-">O-</option>
              	<option value="B-">B-</option>
              	<option value="AB-">AB-</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>3. Urinalysis</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="urinalysis" name="urinalysis">
              	<option value="not required">Not Required</option>
              	<option value="normal">Normal</option>
              	<option value="not normal">Not Normal</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>4. Direct Fecal Smear</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="fecal" name="fecal">
              	<option value="not required">Not Required</option>
              	<option value="normal">Normal</option>
              	<option value="not normal">Not Normal</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>5. VDRL</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="vdrl" name="vdrl">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>6. Hepa B Test</label>
            </div>
            <div class="col-md-7 ">
               <select class="form-control" id="hepab" name="hepab">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>7. AIDS Test</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="aids" name="aids">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>8. Pregnancy Test</label>
            </div>
            <div class="col-md-7 ">
             <select class="form-control" id="pt" name="pt">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12">
              <label>D. Phase II</label>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>1. Hepa C Test</label>
            </div>
            <div class="col-md-7 ">
             <select class="form-control" id="hepac" name="hepac">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>2. TP-EIA</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="tp" name="tp">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
          
            <div class="col-md-4 ml-3 ">
              <label>3. Malarial Smear</label>
            </div>
            <div class="col-md-7 ">
        	  <select class="form-control" id="ms" name="ms">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>4. Stool Culture</label>
            </div>
            <div class="col-md-7">
              <input class="form-control" name="stool" value="{{ old('stool') }}">
            </div>
            <div class="col-md-12 ml-3 ">
              <label>5. Blood Chemistry</label>
            </div>
            <div class="col-md-4 ml-5 ">
              <label for="">A. FBS/ RBS</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="fbs" value="{{ old('fbs') }}">
            </div>
            <div class="col-md-4 ml-5 ">
              <label for="">B. Kidney Function Test</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="kft" value="{{ old('kft') }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">B1. BUN</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="kft_bun" value="{{ old('kft_bun') }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">B2. Serum Creatinine</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="kft_serum" value="{{ old('kft_serum') }}">
            </div>
            <div class="col-md-4 ml-5 ">
              <label for="">C. Liver Function Test</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft" value="{{ old('lft') }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C1. SGOT</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_sgot" value="{{ old('lft_sgot') }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C2. SGPT</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_sgpt" value="{{ old('lft_sgpt') }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C3. Total Billrubin</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_total" value="{{ old('lft_total') }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C4. Indirect Billrubin (B1)</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_indirect" value="{{ old('lft_indirect') }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C5. Direct Billrubin (B1)</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_direct" value="{{ old('lft_direct') }}">
            </div>
            <div class="col-md-4 ml-5 pl-5 ">
              <label for="">C6. Alkaline Phospharase</label>
            </div>
            <div class="col-md-6 ">
              <input type="text" class="form-control" name="lft_alkaline" value="{{ old('lft_alkaline') }}">
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12 ">
              <label>E. Others</label>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>1. Typhoid</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="typhoid" name="typhoid">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>2. Cholera</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="cholera" name="cholera">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>3. Leprosy</label>
            </div>
            <div class="col-md-7 ">
             <select class="form-control" id="leprosy" name="cholera">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
          </div>
          <div class="row ">
            <div class="col-md-12 ">
              <label>F. Drug Test</label>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>1. Alcohol Test</label>
            </div>
            <div class="col-md-7 ">
               <select class="form-control" id="alcohol" name="alcohol">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>2. MET</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="met" name="met">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
            <div class="col-md-4 ml-3 ">
              <label>3. THC</label>
            </div>
            <div class="col-md-7 ">
              <select class="form-control" id="thc" name="thc">
              	<option value="positive">Positive</option>
              	<option value="negative">Negative</option>
              	<option value="not required">Not Required</option>
              </select>
            </div>
          </div>
          <div class="col-md-12">
	            <button  class="btn btn-info">Save</button>
		  </div>
		</form>
	</div>
</div>
@endsection