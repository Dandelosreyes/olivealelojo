@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-4">
	  <div class="card-header">Patients
	</div>
	  <div class="card-body text-dark">
		<table class="table " id="myTable" >
		  <thead class="thead-inverse card-text">
		      <th>#</th>
		      <th>Name</th>
		      <th>Lab Test</th>
		      <th>Date Created</th>
		  </thead>
		  <tbody>
		    @include('shared.alerts')
		    @foreach($patients as $patient)
		    <tr>
		      	<td>{{ $patient->id }}</td>
		      	<td>{{ $patient->lastname }} {{ $patient->firstname }}, {{ $patient->middlename }}</td>
			    <td>
			    	@if($patient->lab->isEmpty())
			    	New <a href="{{ route('labreport.create',['id' => $patient->id]) }}">Record</a>
			    	@else
			    	<a href="{{ route('labreport.edit',['id' => $patient->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
			    	@endif
			    </td>
			    <td>{{ $patient->created_at->format('m-d-Y') }}</td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	  </div>
</div>
</div>
@endsection
