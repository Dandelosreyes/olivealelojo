<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>
    @auth
     <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <a class="navbar-brand" href="#">{{ config('app.name', 'Laravel') }}</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item {{ Nav::isRoute('home','active') }}">
            <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
          </li>
          @if(Auth::user()->role_id == 4)
          <li class="nav-item {{ Nav::isResource('users',null,'active','true') }}">
            <a class="nav-link" href="{{ route('users.index') }}">Employees</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('patients.admin') }}">Patients</a>
          </li>
          @elseif(Auth::user()->role_id == 1)
          <li class="nav-item">
            <a class="nav-link" href="{{ route('information.index') }}">Patient</a>
          </li>


          @elseif(Auth::user()->role_id == 3)
          <li class="nav-item">
            <a class="nav-link" href="{{ route('patients') }}">Patient</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('upload') }}">Lab Image</a>
          </li>
          @elseif(Auth::user()->role_id == 2)
          <li class="nav-item">
            <a class="nav-link" href="{{ route('labtech') }}">Patient</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('upload') }}">Lab Image</a>
          </li>
          @endif
        </ul>
        <ul class="navbar-nav float-right mr-5">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               Welcome, {{ Auth::user()->name }} 
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              @if(Auth::user()->role_id == 4)
              <a class="dropdown-item" href="{{ route('logs') }}">Activity Logs</a>  
              @endif           
               <a class="dropdown-item" href="{{ route('settings') }}">My Password</a>
              <hr> 
              <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout</a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    @endauth

    @yield('content')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
      $('#myTable').DataTable();
    </script>
    @stack('scripts')
</body>
</html>
