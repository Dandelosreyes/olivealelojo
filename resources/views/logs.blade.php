@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-4">
	  <div class="card-header">Activity Log
	</div>
	  <div class="card-body text-dark">
		<table class="table " id="myTable" >
		  <thead class="thead-inverse card-text">
		  	<th>ID</th>
		  	<th>User</th>
		  	<th>Action</th>
		  	<th>Date Created</th>
		  </thead>
		  <tbody>
		  	@foreach($logs as $log)
		  	<tr>
		  		<td>{{ $log->id }}</td>
		  		<td>{{ $log->employee['0']->name }}</td>
		  		<td>{{ $log->action }}</td>
		  		<td>{{ $log->created_at }}</td>
		  	</tr>
		  	@endforeach
		  </tbody>
		</table>
	</div>
	</div>
</div>
@endsection