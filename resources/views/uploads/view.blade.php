@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-4">
	  <div class="card-header">Lab Images <a class="btn btn-primary float-right" href="{{ route('upload') }}">Back</a>
	</div>
	  <div class="card-body text-dark">
		<div class="row">
			<div class="col-12">
				<table class="table table-bordered">
					<thead>
						<th>Category</th>
						<th>Link</th>
						<th>Date Uploaded</th>
					</thead>
					<tbody>
						@foreach($data as $d)
						<tr>
							<td>{{ $d->category }}</td>
							<td><a href="../../image/{{$d->image}}">Image Link</a></td>
							<td>{{ $d->created_at }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	  </div>
</div>
</div>
@endsection
