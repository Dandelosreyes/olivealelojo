@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-dark mt-4">
	  <div class="card-header">Patients
	</div>
	  <div class="card-body text-dark">
		<table class="table " id="myTable" >
		  <thead class="thead-inverse card-text">
		      <th>#</th>
		      <th>Name</th>
		      <th>Lab Test</th>
		      <th>Date Created</th>
		  </thead>
		  <tbody>
		    @include('shared.alerts')
		    @foreach($patients as $patient)
		    <tr>
		      	<td>{{ $patient->id }}</td>
		      	<td>{{ $patient->lastname }} {{ $patient->firstname }}, {{ $patient->middlename }}</td>
			    <td>
			    	@if($patient->lab->isEmpty())
			    		Please fill up the lab forms.
		    		@elseif(Auth::user()->role_id == 2 &&  !$patient->lab->isEmpty())
		    			<a href="{{ route('upload.edit',['id' => $patient->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i></a>
		    			<a href="{{ route('upload.search',['id' => $patient->id]) }}" class="btn btn-sm btn-info"><i class="fa fa-search"></i></a>
		    		@elseif(Auth::user()->role_id == 3 && !$patient->lab->isEmpty())
		    			<a href="{{ route('upload.search',['id' => $patient->id]) }}" class="btn btn-sm btn-info"><i class="fa fa-search"></i></a>
	    			@elseif(Auth::user()->role_id ==  2)
			    		<a href="{{ route('upload.edit',['id' => $patient->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-cloud-upload"></i></a>
			    	@endif
			    </td>
			    </td>
			    <td>{{ $patient->created_at->format('m-d-Y') }}</td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	  </div>
</div>
</div>
@endsection
