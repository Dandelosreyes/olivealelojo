@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card border-primary mt-4 ">	
		<div class="card-header">Patient Information</div>
		<div class="card-body">
			<form class="ml-3 mr-3"  method="POST" action="{{ route('upload.save',$id) }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				@include('shared.alerts')
				<div class="row mt-3">
					<div class="form-group col-12 col-sm-12">
					    <label class="form-control-label" for="lastname">Category:</label>
					    <input type="text" class="form-control" name="category">
					</div>
					<div class="form-group col-12 col-sm-12">
					    <label class="form-control-label" for="lastname">Image:</label>
					    <input type="file" class="form-control" name="image">
					</div>
					<div class="form-group col-12 col-sm-12">
					    <button class="btn btn-info">Save</button>
					</div>
				</div>
			</form>	
		</div>
	</div>
</div>	
@endsection