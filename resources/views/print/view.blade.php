@extends('layouts.app')
@section('content')
<div class="container-fluid mt-3">
	<div class="card border-dark">
		<div class="card-header">Print View <button onclick="window.print();" class="btn btn-sm btn-info float-right">Print</button></div>
		<div class="card-body">
			<div class="row">
      <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 text-center">
        <h1>St. Rita Medical and Diagnostic Center</h1>
        <h5>ACCREDITED BY: Department of Health, POEA, DOLE and MARINA</h5>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12 text-left">
        <label class="font-weight-bold mb-0 modify">MAKATI BRANCH</label>
        <p class="mb-0">1705 - B Dian St. Cor. Finlandia Sts.,</p>
        <p class="mb-0">Brgy. San Isidro, Makati City</p>
        <p class="mb-0">Tel No.: 845-1481/ 845-1482/ 886-5653</p>
        <p class="mb-0">Cell No.: 0919-8339469</p>
        <p class="mb-0">Email Add: smndccebu13@yahoo.com</p>
        <label class="font-weight-bold mb-0 modify">License No.: 13-014-12-RLS-R</label>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12 text-center">
        <label style="color:red" class="mt-4 mb-0 modify">BUREAU VERITAS: ISO 9001;2008</label>
        <p style="color:red" class="mb-0">Certificate Nos.:</p>
        <p style="color:red" class="mb-0">330251A-UK-MAKATI</p>
        <p style="color:red" class="mb-0">330251B-UK-CEBU</p>
        <label style="font-size:25px; color:red" class="font-weight-bold mt-3 iso">ISO ACCREDITED</label>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12 text-left">
        <label class="font-weight-bold mb-0 modify">CEBU BRANCH</label>
        <p class="mb-0">7th Floor Hotel de Mercedez Bldg.</p>
        <p class="mb-0">Pelaez Street, Kalubihan, Cebu City</p>
        <p class="mb-0">Tel No.: (032)253-0120/ (032)254-0085</p>
        <p class="mb-0">Cell No.: 0919-8339469</p>
        <p class="mb-0">Email Add: smndccebu13@yahoo.com</p>
        <label class="font-weight-bold mb-0 modify">License No.: 07-024-13-MF-2</label>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 text-center">
        <h3>Medical Examination Report</h3>
        <hr>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Lastname: </label>
        <p>{{ $patient->lastname }}</p>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Firstname: </label>
        <p>{{ $patient->firstname }} </p>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Middlename: </label>
        <p>{{ $patient->middlename }} </p>
      </div>
      <div class="col-md-12 col-lg-4 col-xl-4 col-sm-12">
        <label>Mailing Address: </label>
        <p>{{ $patient->mailingaddress }} </p>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Passport: </label>
        <p>{{ $patient->passport }} </p>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Gender: </label>
        <p>@if($patient->gender) Male @else Female @endif </p>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Place of Birth: </label>
        <p>{{ $patient->placeofbirth }}</p>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Civil Status: </label>
        <p>{{ $patient->civilstatus }}</p>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Tel #: </label>
        <p>{{ $patient->telnumber }}</p>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Occupation: </label>
        <p>{!! $patient->occupation !!}</p>
      </div>
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-12">
        <label>Destination: </label>
        <p>{{$patient->destination }}</p>
      </div>
    </div>
    <div class="row" style="page-break-after: always">
      <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12">
        <h4>I. Medical History</h4>
        <hr>
      </div>
		@foreach($diseases as $disease)
      	<div class="col-md-4 col-lg-4 col-xl-4 col-sm-6">
			<label for="{{ $disease->id }}" class="control-label">{{ in_array($disease->id,explode(',',$patient->history)) ? 'YES':'NO' }} - {{ $disease->disease }}</label>
		</div>
		@endforeach
    </div>
    <div class="row hidemoto" >
      <div class="col-md-4 col-lg-4 col-xl-4 col-sm-4" style="width: 100px; height:100px; border:1px solid black;">
      </div>
      <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8">
        <p>
          I hereby permit DOH/MARINA/POEA and the undersigned to furnish such
          information the company may need pertaining to my health status and pertinent medical
          findings and do hereby relese them from any and all legal responsibilities by doing so.
          I also certify that my medical history contained above is true and any false statements will
          disqualify me from my employment benefits and claims.
        </p>
      </div>
    </div>
    <div class="row hidemoto" >
      <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12 text-center">
        <input type="text" name="" value="" style="width: 100%; border:none; border-bottom: 1px solid black; margin-top:50px">
        <label>Signature Examinee</label>
      </div>
      <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12 text-center">
        <input type="text" name="" value="" style="width: 100%; border:none; border-bottom: 1px solid black; margin-top:50px">
        <label>Name of Employer/ Agency</label>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12">
        <h4>II. Vital Signs</h4>
        <hr>
      </div>
      <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12">
        <label for="">Height: </label>
        <p>{{ $patient->vitalsign['0']->height }}</p>
      </div>
      <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12">
        <label for="">Weight: </label>
        <p>{{ $patient->vitalsign['0']->weight }}</p>
      </div>
      <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12">
        <label for="">BP: </label>
        <p>{{ $patient->vitalsign['0']->bp }}</p>
      </div>
      <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12">
        <label for="">Pulse: </label>
        <p{{ $patient->vitalsign['0']->pulse }}</p>
      </div>
      <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12">
        <label for="">Respiration: </label>
        <p>{{ $patient->vitalsign['0']->respiration }}</p>
      </div>
      <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12">
        <label for="">BMI: </label>
        <p>{{ $patient->vitalsign['0']->bmi }}</p>
      </div>
      <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12">
        <label for="">PPD: </label>
        <p>{{ $patient->vitalsign['0']->ppd }}</p>
      </div>
      <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12">
        <label for="">MMR: </label>
        <p>{{ $patient->vitalsign['0']->mmr }}</p>
      </div>
      <div class="col-md-3 col-lg-3 col-xl-3 col-sm-12">
        <label for="">Body Built: </label>
        <p>{{ $patient->vitalsign['0']->bodybuilt }}</p>
      </div>

    </div>
    <div class="row">
      <div class="col-md-2 col-lg-2 col-xl-2 col-sm-2">
        <label for="">Visual Aculty</label>
        <p>Uncorrected</p>
        <p>Corrected</p>
      </div>
      <div class="col-md-5 col-lg-5 col-xl-5 col-sm-5">
        <label for="">Far Vision</label>
        <div class="row">
          <div class="col-md-3 col-lg-3 col-xl-3 col-sm-3">
            <p for="">OD</p>
            <p for="">OD</p>
          </div>
          <div class="col-md-3 col-lg-3 col-xl-3 col-sm-3">
            <p>{{ $patient->vitalsign['0']->uncorrected_fv_od }}</p>
            <p>{{ $patient->vitalsign['0']->corrected_fv_os }}</p>
          </div>
          <div class="col-md-3 col-lg-3 col-xl-3 col-sm-3">
            <p for="">OS</p>
            <p for="">OS</p>
          </div>
          <div class="col-md-3 col-lg-3 col-xl-3 col-sm-3">
            <p>{{ $patient->vitalsign['0']->unccorected_fv_os }}</p>
            <p>{{ $patient->vitalsign['0']->corrected_fv_od }}</p>
          </div>
        </div>

      </div>
      <div class="col-md-5 col-lg-5 col-xl-5 col-sm-5">
        <label for="">Near Vision</label>
        <div class="row">
          <div class="col-md-3 col-lg-3 col-xl-3 col-sm-3">
            <p for="">OD</p>
            <p for="">OD</p>
          </div>
          <div class="col-md-3 col-lg-3 col-xl-3 col-sm-3">
             <p>{{ $patient->vitalsign['0']->uncorrected_nv_od }}</p>
            <p>{{ $patient->vitalsign['0']->corrected_nv_os }}</p>
          </div>
          <div class="col-md-3 col-lg-3 col-xl-3 col-sm-3">
            <p for="">OS</p>
            <p for="">OS</p>
          </div>
          <div class="col-md-3 col-lg-3 col-xl-3 col-sm-3">
            <p>{{ $patient->vitalsign['0']->unccorected_nv_os }}</p>
            <p>{{ $patient->vitalsign['0']->corrected_nv_od }}</p>
          </div>
        </div>

      </div>
      <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
        <label for="">Color Vision</label>
        <p>@if($patient->vitalsign['0']->colorvision) For Opto Clearance @else Cleared @endif</p>
      </div>
      <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
        <label for="">Audiometry</label>
        <p>AD: {{ $patient->vitalsign['0']->ad}} - AS: {{ $patient->vitalsign['0']->as }}</p>
      </div>

    </div>
    <div class="row">
      <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Skin</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->skin }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Head, Neck, Scalp</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->neck }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Eyes External</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->pupils }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Pupils, Opthalmoscopic</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->pupils }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Ears</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->ears }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Nose, Sinuses</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->nose }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Mouth, Throat</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->mouth }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Neck, L.N, Thyroid</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->thyroid }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Chest, Breast, Axilla</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->chest}}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Lungs</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->lungs }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Heart</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->heart }}</label>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Abdomen</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->abdomen }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Back</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->back }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Anus Rectum</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->anus }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">G-U System</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->gusystem }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Inguinals, Genitals</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->inguinals }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Reflexes</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->reflexes }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6 ">
            <label for="">Extremities</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->phyexam['0']->extremities }}</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">Dental(teeth)</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">{{ $patient->dental['0']->dentaltx }}</label>
          </div>
        </div>
      </div>

    </div>
    <div class="mt-5"></div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12">
        <h4>III. X-Ray, ECG, Laboratory Examination Report</h4>
        <hr>
      </div>
      <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">A. X-Ray Examination</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->xrayexam }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6 pl-4">
            <label for="">X-Ray No.</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->xrayno }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6 pl-4">
            <label for="">Impression</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->xrayfindings }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">B. ECG Report</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->ecg }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">C. Complete Blood Count</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->cbc }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">D. Urinalysis</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->urinalysis }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">E. Stool Examination</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->directfecalsmear }}</p>
          </div>
        </div>

      </div>
      <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12">
        <div class="row">
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">F. Serological Test(VDRL)</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->vdrl }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">G. HEPA-B Surface Antigen Test</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->hepabtest }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">H. AIDS Clearance Test</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->aidstest }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">I. Blood Type</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->bloodtype }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">J. Psychological Test</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->psychotest }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">K. Drug Test</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>Alcohol Test:{{ $patient->lab['0']->alcoholtest }}-MET:{{ $patient->lab['0']->met }}-THC: {{ $patient->lab['0']->thc }}</p>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <label for="">E. Pregnancy Test</label>
          </div>
          <div class="col-md-6 col-lg-6 col-xl-6 col-sm-6">
            <p>{{ $patient->lab['0']->pregnancytest }}</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12">
        <hr>
        <h4>Remarks</h4>
        <p>
        	  Remarks: <b>{{ $patient->lab['0']->remarks }}</b><br>
            Psychological Test: <b>{{ $patient->lab['0']->psychotest }}</b><br>
            Suggestion: <b>{{ $patient->lab['0']->suggestion }}</b>
        </p>
        <hr>
      </div>
    </div>
    <div class="row">


   
   <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12">
      <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 text-center">
          <input type="text" name="" value="{{ Auth::user()->name }}" style="width: 100%;text-align: center; border:none; border-bottom: 1px solid black;">
          <label>Examining Physician:</label>
      </div>
    </div>
   <div class="col-md-6 col-lg-6 col-xl-6 col-sm-12">
      <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 text-center">
        <input type="text" name="" value="{{ \Carbon\Carbon::now() }}" style="width: 100%; border:none;text-align: center; border-bottom: 1px solid black;">
        <label>Date</label>
      </div>
    </div>
        
    </div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<style>
@media print{
	.navbar{
		display:none;
	}
   .card{
   	border:none;
   }
   .card-header{
   	display:none;
   }
  .col-md-7{
    width: 60% !important;
  }
  .col-md-2{
    width: auto;
  }
  .col-md-5{
    width: 40%;
    float: left !important;
  }
  .col-md-6{
    width: 50%;
  }
  .col-md-8{
    width: 80%;
    float: left;
  }
  .col-md-1{
    width: 10%;
  }
  .col-md-3{
    width: 20%;
  }
  .col-md-4{
    width: 33.3333333%;
  }
  label, p{
    font-size: 10px;
    margin-top: 0px !important;
    margin-bottom: 0px !important;
    }
  .iso{
    font-size: 14px;
    margin-top: 0px !important;
    margin-bottom: 0px !important;
  }
  .modify{
    font-size: 10px;
    margin-top: 0px !important;
    margin-bottom: 0px !important;
  }
  h1{
    font-size: 30px;
  }
  h5{
    font-size: 15px;
  }
  h3{
    font-size: 25px;
  }
  h4{
    font-size: 18px;
  }
  .label-modify{
    margin-top: 60px !important;
  }
  .pb
  {
  	page-break-after: auto;
  }
  .mt-5{
  	display: block; page-break-before: auto;
  }
  .hidemoto{
    display:flex !important;
  }
}
</style>
<style type="text/css">
   .hidemoto{
    display:none;
  }
</style>
@endpush