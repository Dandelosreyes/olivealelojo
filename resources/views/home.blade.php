@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 mx-auto mt-5">
            <div class="card card-default">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @include('shared.alerts')
                    <div class="col-12">
                         <div class="text-center">
                            <h5>Welcome to St. Rita Clinic</h5>
                            <img src="{{ asset('image/logo.jpg') }}">
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
