@extends('layouts.app')
@section('content')
<div class ="container">
	<div class="card card-info mt-3">
		<div class="card-header">Dental Examination</div>
		<div class="card-body">
			@include('shared.alerts')
			<form method="POST" action="{{ route('dental.store',['id' => $id]) }}">
				<div class="row">
				<div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
					<table class="table table-hover table-bordered">
					    <thead>
					      <tr>
					        <th class="text-center">Teeth Position</th>
					        <th class="text-center">8</th>
					        <th class="text-center">7</th>
					        <th class="text-center">6</th>
					        <th class="text-center">5</th>
					        <th class="text-center">4</th>
					        <th class="text-center">3</th>
					        <th class="text-center">2</th>
					        <th class="text-center">1</th>
					      </tr>
					    </thead>
					    <tbody>
					    	<tr>
					    		<td>Upper Left</td>
					    		<td onclick="test(this.id)" id="ul8">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperleft8" @if(old('upperleft8')) 
								  			@push('scripts') <script>$('#ul8').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>		
								</td>
				    			<td onclick="test(this.id)" id="ul7">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperleft7" @if(old('upperleft7')) 
								  			@push('scripts') <script>$('#ul7').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ul6">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperleft6" @if(old('upperleft6')) 
								  			@push('scripts') <script>$('#ul6').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ul5">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperleft5" @if(old('upperleft5')) 
								  			@push('scripts') <script>$('#ul5').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ul4">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperleft4" @if(old('upperleft4')) 
								  			@push('scripts') <script>$('#ul4').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ul3">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperleft3" @if(old('upperleft3')) 
								  			@push('scripts') <script>$('#ul3').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ul2">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperleft2" @if(old('upperleft2')) 
								  			@push('scripts') <script>$('#ul2').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ul1">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperleft1" @if(old('upperleft1')) 
								  			@push('scripts') <script>$('#ul1').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>	
							</tr>
					    	<tr>
					    		<td>Lower Left</td>
					    		<td onclick="test(this.id)" id="ll8">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerleft8" @if(old('lowerleft8')) 
								  			@push('scripts') <script>$('#ll8').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>		
								</td>
				    			<td onclick="test(this.id)" id="ll7">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerleft7" @if(old('lowerleft7')) 
								  			@push('scripts') <script>$('#ll7').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ll6">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerleft6" @if(old('lowerleft6')) 
								  			@push('scripts') <script>$('#ll6').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ll5">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerleft5" @if(old('lowerleft5')) 
								  			@push('scripts') <script>$('#ll5').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ll4">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerleft4" @if(old('lowerleft4')) 
								  			@push('scripts') <script>$('#ll4').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ll3">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerleft3" @if(old('lowerleft3')) 
								  			@push('scripts') <script>$('#ll3').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ll2">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerleft2" @if(old('lowerleft2')) 
								  			@push('scripts') <script>$('#ll2').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ll1">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerleft1" @if(old('lowerleft1')) 
								  			@push('scripts') <script>$('#ll1').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>	
							</tr>																				
					    </tbody>
				    </table>
					<table class="table table-hover table-bordered">
					    <thead>
					      <tr>
					        <th class="text-center">Teeth Position</th>
					        <th class="text-center">8</th>
					        <th class="text-center">7</th>
					        <th class="text-center">6</th>
					        <th class="text-center">5</th>
					        <th class="text-center">4</th>
					        <th class="text-center">3</th>
					        <th class="text-center">2</th>
					        <th class="text-center">1</th>
					      </tr>
					    </thead>
					    <tbody>
					    	<tr>
					    		<td>Upper Right</td>
					    		<td onclick="test(this.id)" id="ur8">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperright8" @if(old('upperright8')) 
								  			@push('scripts') <script>$('#ur8').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>		
								</td>
				    			<td onclick="test(this.id)" id="ur7">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperright7" @if(old('upperright7')) 
								  			@push('scripts') <script>$('#ur7').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ur6">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperright6" @if(old('upperright6')) 
								  			@push('scripts') <script>$('#ur6').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ur5">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperright5" @if(old('upperright5')) 
								  			@push('scripts') <script>$('#ur5').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ur4">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperright4" @if(old('upperright4')) 
								  			@push('scripts') <script>$('#ur4').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ur3">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperright3" @if(old('upperright3')) 
								  			@push('scripts') <script>$('#ur3').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ur2">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperright2" @if(old('upperright2')) 
								  			@push('scripts') <script>$('#ur2').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="ur1">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="upperright1" @if(old('upperright1')) 
								  			@push('scripts') <script>$('#ur1').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>	
							</tr>
					    	<tr>
					    		<td>Lower Right</td>
					    		<td onclick="test(this.id)" id="lr8">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerright8" @if(old('lowerright8')) 
								  			@push('scripts') <script>$('#lr8').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>		
								</td>
				    			<td onclick="test(this.id)" id="lr7">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerright7" @if(old('lowerright7')) 
								  			@push('scripts') <script>$('#lr7').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="lr6">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerright6" @if(old('lowerright6')) 
								  			@push('scripts') <script>$('#lr6').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="lr5">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerright5" @if(old('lowerright5')) 
								  			@push('scripts') <script>$('#lr5').css('background-color','#3498db');</script> @endpush checked  @endif ></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="lr4">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerright4" @if(old('lowerright4')) 
								  			@push('scripts') <script>$('#lr4').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="lr3">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerright3" @if(old('lowerright3')) 
								  			@push('scripts') <script>$('#lr3').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="lr2">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerright2" @if(old('lowerright2')) 
								  			@push('scripts') <script>$('#lr2').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
				    			<td onclick="test(this.id)" id="lr1">								    	
					    			<div class="checkbox">
								  		<label><input type="checkbox" value="1" name="lowerright1" @if(old('lowerright1')) 
								  			@push('scripts') <script>$('#lr1').css('background-color','#3498db');</script> @endpush checked  @endif></label>
									</div>
								</td>
							</tr>																				
					    </tbody>
				    </table>
				</div>
				<div class="clearfix"></div>
				<div class="form-group ml-3">
					<label class="form-control-label" for="lastname">Dental Tx:</label>
				    <input type="text" class="form-control" id="dentalTx" name="dentalTx" value="{{ old('dentalTx') }}">
				</div>
				<div class='form-group col-xs-12 col-sm-12 col-md-12'>
					{{ csrf_field() }}
					<button type="submit" class="btn btn-next">Save</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
@endsection
@push('styles')
	<style type="text/css">
		.checkbox label input[type=checkbox]
		{
			display: none;
		}
	</style>
@endpush
@push('scripts')
	<script>
		function test(id)
		{
			$("#"+id+ " .checkbox input[type=checkbox]").trigger('click');
			var checkbox = $("#"+id+ " .checkbox input[type=checkbox]").prop('checked');
			if(checkbox)
			{
				$('#'+id).css('background-color','#3498db');
			}
			else{
				$('#'+id).css('background-color','white');
			}
		}
	</script>
@endpush