<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LabImage extends Model
{
    public function lab()
    {
    	return $this->belongsTo('App/Models/Information');
    }
}
