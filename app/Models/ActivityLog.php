<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    protected $fillable = ['user_id' ,'action'];
    public function employee(){
    	return $this->hasMany('App\User','id','user_id');
    }
}
