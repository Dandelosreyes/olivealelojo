<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Information extends Model
{
    public function getAgeAttribute()
	{
	    return Carbon::parse($this->attributes['birthday'])->age;
	}
	public function vitalsign()
	{
		return $this->hasMany('App\Models\Vitalsign','information_id');
	}
	public function dental()
	{
		return $this->hasMany('App\Models\Dental','information_id');
	}
	public function phyexam()
	{
		return $this->hasMany('App\Models\Physicalexamination','information_id');
	}
	public function lab()
	{
		return $this->hasMany('App\Models\LabReport','information_id');
	}
}
