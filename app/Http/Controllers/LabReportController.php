<?php

namespace App\Http\Controllers;

use App\Models\LabReport;
use Illuminate\Http\Request;
use Auth;
use App\Models\LabImage;
class LabReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('lab.create')->with('id',$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $request->validate(
            [
                'xrayno' => 'required|integer',
                'xrayfindings' => 'required',
                'ecg' => 'required',
                'cbc_hgb' => 'required',
                'fbs' => 'required',
                'kft' => 'required',
                'kft_bun' =>'required',
                'kft_serum' => 'required',
                'lft' => 'required',
                'lft_sgot' =>'required',
                'lft_sgpt' =>'required',
                'lft_total' => 'required',
                'lft_indirect' => 'required',
                'lft_direct' => 'required',
                'lft_alkaline' => 'required',
                'stool' => 'required'
            ]
        );
         $lab = new LabReport;
         $lab->information_id = $id;
         $lab->xrayno = $request->xrayno;
         $lab->xrayexam = $request->xrayexam;
         $lab->xrayfindings = $request->xrayfindings;
         $lab->cbc = $request->cbc;
         $lab->cbc_hgb = $request->cbc_hgb;
         $lab->bloodtype = $request->bloodtype;
         $lab->urinalysis = $request->urinalysis;
         $lab->directfecalsmear = $request->fecal;
         $lab->vdrl = $request->vdrl;
         $lab->hepabtest = $request->hepab;
         $lab->aidstest = $request->aids;
         $lab->pregnancytest = $request->pt;
         $lab->thyphoid = $request->typhoid;
         $lab->cholera = $request->cholera;
         $lab->leprosy = $request->leprosy;
         $lab->alcoholtest = $request->alcohol;
         $lab->met = $request->met;
         $lab->thc = $request->thc;
         $lab->ecg = $request->ecg;
         $lab->hepactest = $request->hepac;
         $lab->tp_eia = $request->tp;
         $lab->malarialsmear = $request->ms;
         $lab->stoolculture = $request->stool;
         $lab->fbs_rbs = $request->fbs;
         $lab->kidneyfunctest = $request->kft;
         $lab->kidneyfunctest_bun = $request->kft_bun;
         $lab->kidneyfunctest_serum = $request->kft_serum ;
         $lab->liverfunctest = $request->lft;
         $lab->liverfunctest_sgot = $request->lft_sgot;
         $lab->liverfunctest_sgpt = $request->lft_sgpt;
         $lab->liverfunctest_billirubin  = $request->lft_total;
         $lab->liverfunctest_indirect = $request->lft_indirect;
         $lab->liverfunctest_direct = $request->lft_direct;
         $lab->liverfunctest_alkaline = $request->lft_alkaline;
         $lab->save();
         \App\Models\ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Created a lab report for patient '.$id]);
         return redirect()->route('labtech')->with('status','Record added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LabReport  $labReport
     * @return \Illuminate\Http\Response
     */
    public function show(LabReport $labReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LabReport  $labReport
     * @return \Illuminate\Http\Response
     */
    public function edit(LabReport $labReport,$id)
    {
        $labReport = LabReport::where('information_id',$id)->first();
        return view('lab.edit')->with('patient',$labReport);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LabReport  $labReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LabReport $labReport,$id)
    {
        $request->validate(
            [
                'xrayno' => 'required|integer',
                'xrayfindings' => 'required',
                'ecg' => 'required',
                'cbc_hgb' => 'required',
                'fbs' => 'required',
                'kft' => 'required',
                'kft_bun' =>'required',
                'kft_serum' => 'required',
                'lft' => 'required',
                'lft_sgot' =>'required',
                'lft_sgpt' =>'required',
                'lft_total' => 'required',
                'lft_indirect' => 'required',
                'lft_direct' => 'required',
                'lft_alkaline' => 'required',
                'stool' => 'required'
            ]
        );
         $lab = LabReport::find($id);
         $lab->xrayno = $request->xrayno;
         $lab->xrayexam = $request->xrayexam;
         $lab->xrayfindings = $request->xrayfindings;
         $lab->cbc = $request->cbc;
         $lab->cbc_hgb = $request->cbc_hgb;
         $lab->bloodtype = $request->bloodtype;
         $lab->urinalysis = $request->urinalysis;
         $lab->directfecalsmear = $request->fecal;
         $lab->vdrl = $request->vdrl;
         $lab->hepabtest = $request->hepab;
         $lab->aidstest = $request->aids;
         $lab->pregnancytest = $request->pt;
         $lab->thyphoid = $request->typhoid;
         $lab->cholera = $request->cholera;
         $lab->leprosy = $request->leprosy;
         $lab->alcoholtest = $request->alcohol;
         $lab->met = $request->met;
         $lab->thc = $request->thc;
         $lab->ecg = $request->ecg;
         $lab->hepactest = $request->hepac;
         $lab->tp_eia = $request->tp;
         $lab->malarialsmear = $request->ms;
         $lab->stoolculture = $request->stool;
         $lab->fbs_rbs = $request->fbs;
         $lab->kidneyfunctest = $request->kft;
         $lab->kidneyfunctest_bun = $request->kft_bun;
         $lab->kidneyfunctest_serum = $request->kft_serum ;
         $lab->liverfunctest = $request->lft;
         $lab->liverfunctest_sgot = $request->lft_sgot;
         $lab->liverfunctest_sgpt = $request->lft_sgpt;
         $lab->liverfunctest_billirubin  = $request->lft_total;
         $lab->liverfunctest_indirect = $request->lft_indirect;
         $lab->liverfunctest_direct = $request->lft_direct;
         $lab->liverfunctest_alkaline = $request->lft_alkaline;
         if(Auth::user()->role_id == 3)
        {
         $lab->psychotest = $request->psychotest;
         $lab->remarks = $request->remarks;
         $lab->suggestion = $request->suggestion;   
        }
         $lab->save();
         \App\Models\ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Updated a lab report for patient '.$id]);
         return redirect()->route('labtech')->with('status','Lab report updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LabReport  $labReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(LabReport $labReport)
    {
        //
    }
    public function upload_view()
    {
        return view('uploads/index')->with(['patients' => \App\Models\Information::all()]);  
    }
    public function select($id)
    {
        return view('uploads/create')->with('id',$id);
    }
    public function saveimage(Request $request,$id)
    {
        $request->validate([
            'category' => 'required',
            'image' => 'required|image'
        ]);
        $labRep = 
        $photoName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('image'), $photoName);
        $lab = new LabImage;
        $lab->image = $photoName;
        $lab->category = $request->category;
        $lab->information_id = $id;
        $lab->save();
        \App\Models\ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Uploaded an image of lab report for patient '.$id]);
         return redirect()->back()->with('status','File Uploaded');
    }
    public function viewImage($id)
    {
        $data = LabImage::find($id)->all();
        return view('uploads.view')->with('data',$data);
    }
}
