<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ActivityLog;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function patients()
    {
        return view('patients')->with(['patients' => \App\Models\Information::all()]);  
    }
    public function lab()
    {
        return view('lab')->with(['patients' => \App\Models\Information::all()]);  
    }
    public function print($id)
    {
        return view('print.view')->with(['diseases' => \App\Models\Disease::all() , 'patient' => \App\Models\Information::find($id)]);
    }
    public function logs()
    {
        return view('logs')->with('logs',ActivityLog::all());
    }
    public function view_settings()
    {
        return view('admin/settings');
    }
    public function password(Request $request,$id)
    {
        $request->validate([
            'password' => 'required|confirmed'
        ]);
        $user = User::find($id);
        $user->password = $request->password;
        $user->save();
        return back()->with('status','Password updated.');
    }
    public function employee($id)
    {
        return view('admin/reset')->with('id',$id);
    }
}
