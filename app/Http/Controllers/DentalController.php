<?php

namespace App\Http\Controllers;

use App\Models\Dental;
use Illuminate\Http\Request;
use Auth;
class DentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('dental.create')->with('id',$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $request->validate(['dentalTx' => 'required']);
        $patient = new Dental;
        $patient->information_id = $id;
        $patient->upper_right_8 = $request->upperright8 ?: '0';
        $patient->upper_right_7 = $request->upperright7 ?: '0';
        $patient->upper_right_6 = $request->upperright6 ?: '0';
        $patient->upper_right_5 = $request->upperright5 ?: '0';
        $patient->upper_right_4 = $request->upperright4 ?: '0';
        $patient->upper_right_3 = $request->upperright3 ?: '0';
        $patient->upper_right_2 = $request->upperright2 ?: '0';
        $patient->upper_right_1 = $request->upperright1 ?: '0';
        $patient->upper_left_8 = $request->upperleft8 ?: '0';
        $patient->upper_left_7 = $request->upperleft7 ?: '0';
        $patient->upper_left_6 = $request->upperleft6 ?: '0';
        $patient->upper_left_5 = $request->upperleft5 ?: '0';
        $patient->upper_left_4 = $request->upperleft4 ?: '0';
        $patient->upper_left_3 = $request->upperleft3 ?: '0';
        $patient->upper_left_2 = $request->upperleft2 ?: '0';
        $patient->upper_left_1 = $request->upperleft1 ?: '0';
        $patient->lower_right_8 = $request->lowerright8 ?: '0';
        $patient->lower_right_7 = $request->lowerright7 ?: '0';
        $patient->lower_right_6 = $request->lowerright6 ?: '0';
        $patient->lower_right_5 = $request->lowerright5 ?: '0';
        $patient->lower_right_4 = $request->lowerright4 ?: '0';
        $patient->lower_right_3 = $request->lowerright3 ?: '0';
        $patient->lower_right_2 = $request->lowerright2 ?: '0';
        $patient->lower_right_1 = $request->lowerright1 ?: '0';
        $patient->lower_left_8 = $request->lowerleft8 ?: '0';
        $patient->lower_left_7 = $request->lowerleft7 ?: '0';
        $patient->lower_left_6 = $request->lowerleft6 ?: '0';
        $patient->lower_left_5 = $request->lowerleft5 ?: '0';
        $patient->lower_left_4 = $request->lowerleft4 ?: '0';
        $patient->lower_left_3 = $request->lowerleft3 ?: '0';
        $patient->lower_left_2 = $request->lowerleft2 ?: '0';
        $patient->lower_left_1 = $request->lowerleft1 ?: '0';
        $patient->dentaltx = $request->dentalTx;
        $patient->save();
        \App\Models\ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Created a dental checkup for patient '.$id]);
        return redirect()->route('patients')->with('status','Dental Checkup added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dental  $dental
     * @return \Illuminate\Http\Response
     */
    public function show(Dental $dental)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dental  $dental
     * @return \Illuminate\Http\Response
     */
    public function edit(Dental $dental,$id)
    {
        $dental = Dental::where('information_id','=',$id)->first();
        return view('dental.edit')->with('dental',$dental);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dental  $dental
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dental $dental,$id)
    {
        $dental = Dental::find($id);
        $dental->upper_right_8 = $request->upperright8 ?: '0';
        $dental->upper_right_7 = $request->upperright7 ?: '0';
        $dental->upper_right_6 = $request->upperright6 ?: '0';
        $dental->upper_right_5 = $request->upperright5 ?: '0';
        $dental->upper_right_4 = $request->upperright4 ?: '0';
        $dental->upper_right_3 = $request->upperright3 ?: '0';
        $dental->upper_right_2 = $request->upperright2 ?: '0';
        $dental->upper_right_1 = $request->upperright1 ?: '0';
        $dental->upper_left_8 = $request->upperleft8 ?: '0';
        $dental->upper_left_7 = $request->upperleft7 ?: '0';
        $dental->upper_left_6 = $request->upperleft6 ?: '0';
        $dental->upper_left_5 = $request->upperleft5 ?: '0';
        $dental->upper_left_4 = $request->upperleft4 ?: '0';
        $dental->upper_left_3 = $request->upperleft3 ?: '0';
        $dental->upper_left_2 = $request->upperleft2 ?: '0';
        $dental->upper_left_1 = $request->upperleft1 ?: '0';
        $dental->lower_right_8 = $request->lowerright8 ?: '0';
        $dental->lower_right_7 = $request->lowerright7 ?: '0';
        $dental->lower_right_6 = $request->lowerright6 ?: '0';
        $dental->lower_right_5 = $request->lowerright5 ?: '0';
        $dental->lower_right_4 = $request->lowerright4 ?: '0';
        $dental->lower_right_3 = $request->lowerright3 ?: '0';
        $dental->lower_right_2 = $request->lowerright2 ?: '0';
        $dental->lower_right_1 = $request->lowerright1 ?: '0';
        $dental->lower_left_8 = $request->lowerleft8 ?: '0';
        $dental->lower_left_7 = $request->lowerleft7 ?: '0';
        $dental->lower_left_6 = $request->lowerleft6 ?: '0';
        $dental->lower_left_5 = $request->lowerleft5 ?: '0';
        $dental->lower_left_4 = $request->lowerleft4 ?: '0';
        $dental->lower_left_3 = $request->lowerleft3 ?: '0';
        $dental->lower_left_2 = $request->lowerleft2 ?: '0';
        $dental->lower_left_1 = $request->lowerleft1 ?: '0';
        $dental->dentaltx = $request->dentalTx;
        $dental->save() ?: '0';
        \App\Models\ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Updated a dental checkup for patient '.$id]);
        return redirect()->back()->with('status','Dental Checkup Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dental  $dental
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dental $dental)
    {
        //
    }
}
