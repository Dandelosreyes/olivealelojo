<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Models\ActivityLog;
use Auth;
use App\Models\Information;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.index')->with('users',User::where('role_id','!=','4')->with('roles')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create')->with('roles',\App\Models\Role::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'licensenumber' => 'required|unique:users,licensenumber',
            'password' => 'required|confirmed'
        ]);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->licensenumber = $request->licensenumber;
        $user->password = $request->password;
        $user->role_id = $request->role;
        $user->save();
        ActivityLog::create(['user_id' => Auth::user()->id, 'action' => 'Created a employee with the id of '.$user->id]);
        return back()->with('status','Employee added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('employees.view')->with(['roles' => \App\Models\Role::all() , 'user' => $user ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('employees.edit')->with(['roles' => \App\Models\Role::all() , 'user' => $user ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'licensenumber' => 'required|unique:users,licensenumber,'.$user->id
        ]);
        $user->name = $request->name;
        $user->role_id = $request->role;
        $user->licensenumber = $request->licensenumber;
        $user->save();
        ActivityLog::create(['user_id' => Auth::user()->id, 'action' => 'Updated a employee with the id of '.$user->id]);
        return back()->with('status','Employee information updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return back()->with('status','User removed successfully!.');
    }
    public function patients()
    {
        return view('admin/index')->with('patients',Information::all());
    }
    public function deletePatient($id)
    {
        Information::find($id)->delete();
        ActivityLog::create(['user_id' => Auth::user()->id, 'action' => 'Deleted a patient with the id of '.$id]);
        return back()->with('status','Patient removed.');
    }
}
