<?php

namespace App\Http\Controllers;

use App\Models\Physicalexamination;
use Illuminate\Http\Request;
use Auth;
use App\Models\ActivityLog;
class PeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('pe.create')->with('id',$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id )
    {
        $request->validate([
            'skin' => 'required',
            'neck' => 'required',
            'pupils' => 'required',
            'ears' => 'required',
            'nose' => 'required',
            'mouth' => 'required',
            'thyroid' => 'required',
            'chest' => 'required',
            'lungs' => 'required',
            'heart' => 'required',
            'abdomen' => 'required',
            'back' => 'required',
            'anus' => 'required',
            'gusystem' => 'required',
            'inguinals' => 'required',
            'reflexes' => 'required',
            'extremities' => 'required'
        ]);
        $patient = new Physicalexamination;
        $patient->information_id = $id;
        $patient->skin = $request->skin;
        $patient->neck = $request->neck;
        $patient->pupils = $request->pupils;
        $patient->ears = $request->ears;
        $patient->nose = $request->nose;
        $patient->mouth = $request->mouth;
        $patient->thyroid = $request->thyroid;
        $patient->chest = $request->chest;
        $patient->lungs = $request->lungs;
        $patient->heart = $request->heart;
        $patient->abdomen = $request->abdomen;
        $patient->back = $request->back;
        $patient->anus = $request->anus;
        $patient->gusystem = $request->gusystem;
        $patient->inguinals = $request->inguinals;
        $patient->reflexes = $request->reflexes;
        $patient->extremities = $request->extremities;
        $patient->save();
        ActivityLog::create(['user_id' => Auth::user()->id, 'action' => 'Created a physicalexamination for patient '.$id]);
        return redirect()->route('patients')->With('status','Physical Examination record for patient '.$id.' added');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Physicalexamination  $physicalexamination
     * @return \Illuminate\Http\Response
     */
    public function show(Physicalexamination $physicalexamination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Physicalexamination  $physicalexamination
     * @return \Illuminate\Http\Response
     */
    public function edit(Physicalexamination $physicalexamination,$id)
    {
        $physicalexamination = Physicalexamination::where('information_id',$id)->first();
        return view('pe.edit')->with(['patient' => $physicalexamination]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Physicalexamination  $physicalexamination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Physicalexamination $physicalexamination,$id)
    {
        $request->validate([
            'skin' => 'required',
            'neck' => 'required',
            'pupils' => 'required',
            'ears' => 'required',
            'nose' => 'required',
            'mouth' => 'required',
            'thyroid' => 'required',
            'chest' => 'required',
            'lungs' => 'required',
            'heart' => 'required',
            'abdomen' => 'required',
            'back' => 'required',
            'anus' => 'required',
            'gusystem' => 'required',
            'inguinals' => 'required',
            'reflexes' => 'required',
            'extremities' => 'required'
        ]);
        $physicalexamination = Physicalexamination::find($id);
        $physicalexamination->skin = $request->skin;
        $physicalexamination->neck = $request->neck;
        $physicalexamination->pupils = $request->pupils;
        $physicalexamination->ears = $request->ears;
        $physicalexamination->nose = $request->nose;
        $physicalexamination->mouth = $request->mouth;
        $physicalexamination->thyroid = $request->thyroid;
        $physicalexamination->chest = $request->chest;
        $physicalexamination->lungs = $request->lungs;
        $physicalexamination->heart = $request->heart;
        $physicalexamination->abdomen = $request->abdomen;
        $physicalexamination->back = $request->back;
        $physicalexamination->anus = $request->anus;
        $physicalexamination->gusystem = $request->gusystem;
        $physicalexamination->inguinals = $request->inguinals;
        $physicalexamination->reflexes = $request->reflexes;
        $physicalexamination->extremities = $request->extremities;
        $physicalexamination->save();
        ActivityLog::create(['user_id' => Auth::user()->id, 'action' => 'Updated a physicalexamination for patient '.$id]);
        return redirect()->back()->with('status','Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Physicalexamination  $physicalexamination
     * @return \Illuminate\Http\Response
     */
    public function destroy(Physicalexamination $physicalexamination)
    {
        //
    }
}
