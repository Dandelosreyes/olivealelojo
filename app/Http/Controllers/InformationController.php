<?php

namespace App\Http\Controllers;

use App\Models\Information;
use Illuminate\Http\Request;
use App\Rules\ValidPhone;
use App\Rules\ValidBirthday;
use Auth;
use App\Models\ActivityLog;
use Carbon\Carbon;
class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('information.index')->with('patients',Information::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('information.create')->with('diseases',\App\Models\Disease::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'phonenum' => [ 'required',new ValidPhone,'min:13','max:13','unique:information,telnumber'],
            'lastname' => 'required',
            'middlename' => 'required',
            'firstname' => 'required',
            'mailingaddress' => 'required',
            'passport' => 'required|unique:information,passport',
            'gender' => 'required|between:1,2',
            'birthday' => [ 'required','before:'.Carbon::now()],
            'placeofbirth' => 'required',
            'occupation' => 'required',
            'destination' => 'required',
            'civilstatus' => 'required|between:1,4'
        ]);
        $diseases = '';
        if($request->disease)
        {
            $len = count($request->disease);
            $x = 0;
            foreach($request->disease as $sakit)
            {

                if($x != $len - 1)
                {
                    $diseases .= $sakit . ',';
                }
                else
                {
                    $diseases .= $sakit;    
                }
                $x++;
            }
        }
        $civilstatus = '';
        switch ($request->civilstatus) {
            case 1:
                $civilstatus = 'Single';
                break;
            case 2:
                $civilstatus = 'Married';
                break;
            case 3:
                $civilstatus = 'Divorced';            
                break;
            case 4:
                $civilstatus = 'Widow';
                break;
        }
        $patient = new Information;
        $patient->lastname = $request->lastname;
        $patient->middlename = $request->middlename;
        $patient->firstname = $request->firstname;
        $patient->mailingaddress = $request->mailingaddress;
        $patient->passport = $request->passport;
        $patient->gender = $request->gender;
        $patient->birthday = $request->birthday;
        $patient->placeofbirth = $request->placeofbirth;
        $patient->occupation = $request->occupation;
        $patient->destination = $request->destination;
        $patient->civilstatus = $civilstatus;
        $patient->history = $diseases;
        $patient->telnumber = $request->phonenum;
        $patient->save();
        ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Created a patient with the id of '.$patient->id]);
        return redirect()->route('information.index')->with('status','Patient Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function show(Information $information)
    {
    
        return  view('information.view')->with([ 'patient' => $information , 'diseases' => \App\Models\Disease::all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit(Information $information)
    {
        return view('information.edit')->with(['patient' => $information , 'diseases' => \App\Models\Disease::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Information $information)
    {
        $request->validate([
            'phonenum' => [ 'required',new ValidPhone,'min:13','max:13','unique:information,telnumber,'.$information->id],
            'lastname' => 'required',
            'middlename' => 'required',
            'firstname' => 'required',
            'mailingaddress' => 'required',
            'passport' => 'required|unique:information,passport,'.$information->id,
            'gender' => 'required|between:1,2',
            'birthday' => [ 'required','before:'.Carbon::now()],
            'placeofbirth' => 'required',
            'occupation' => 'required',
            'destination' => 'required',
            'civilstatus' => 'required|between:1,4'
        ]);
        $diseases = '';
        if($request->disease)
        {
            $len = count($request->disease);
            $x = 0;
            foreach($request->disease as $sakit)
            {

                if($x != $len - 1)
                {
                    $diseases .= $sakit . ',';
                }
                else
                {
                    $diseases .= $sakit;    
                }
                $x++;
            }
            $information->history = $diseases;
        }
        $civilstatus = '';
        switch ($request->civilstatus) {
            case 1:
                $civilstatus = 'Single';
                break;
            case 2:
                $civilstatus = 'Married';
                break;
            case 3:
                $civilstatus = 'Divorced';            
                break;
            case 4:
                $civilstatus = 'Widow';
                break;
        }
        $information->lastname = $request->lastname;
        $information->middlename = $request->middlename;
        $information->firstname = $request->firstname;
        $information->mailingaddress = $request->mailingaddress;
        $information->passport = $request->passport;
        $information->gender = $request->gender;
        $information->birthday = $request->birthday;
        $information->placeofbirth = $request->placeofbirth;
        $information->occupation = $request->occupation;
        $information->destination = $request->destination;
        $information->civilstatus = $civilstatus;
        $information->telnumber = $request->phonenum;
        $information->save();
        ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Created a patient with the id of '.$information->id]);
        return redirect()->back()->with('status','Patient updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy(Information $information)
    {
        //
    }
}
