<?php

namespace App\Http\Controllers;

use App\Models\Vitalsign;
use Illuminate\Http\Request;
use Auth;
use App\Models\ActivityLog;
class VitalSignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        // dd($id);
        return view('vitalsign.create')->with(['id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $request->validate([
            'height' => 'required',
            'weight' => 'required',
            'bp'     => 'required',
            'pulse' => 'required',
            'respiration' => 'required',
            'bmi' => 'required',
            'ppd' => 'required',
            'mmr' => 'required',
            'bodybuilt' => 'required',
            'lmp' => 'nullable',
            'lsc' => 'nullable',
            'method' => 'nullable',
            'uc_fv_od' => 'required|integer',
            'uc_fv_os' => 'required|integer',
            'c_fv_od' => 'required|integer',
            'c_fv_os' => 'required|integer',
            'uc_nv_od' => 'required|integer',
            'uc_nv_os' => 'required|integer',
            'c_nv_od' => 'required|integer',
            'c_nv_os' => 'required|integer',
            'optical' => 'required|boolean',
            'colorvision' => 'required|boolean',
            'ad' => 'required|integer',
            'as' => 'required|integer'
        ]);
        $patient = new Vitalsign;
        $patient->information_id = $id;
        $patient->height = $request->height;
        $patient->weight = $request->weight;
        $patient->bp = $request->bp;
        $patient->pulse = $request->pulse;
        $patient->respiration = $request->respiration;
        $patient->bmi = $request->bmi;
        $patient->ppd = $request->ppd;
        $patient->lmp = $request->lmp;
        $patient->lsc = $request->lsc;
        $patient->bodybuilt = $request->bodybuilt;
        $patient->mmr = $request->mmr;
        $patient->method = $request->method;
        $patient->uncorrected_fv_od = $request->uc_fv_od;
        $patient->unccorected_fv_os = $request->uc_fv_os;
        $patient->uncorrected_nv_od = $request->uc_nv_od;
        $patient->unccorected_nv_os = $request->uc_nv_os;
        $patient->corrected_fv_od = $request->c_fv_od;
        $patient->corrected_fv_os = $request->c_fv_os;
        $patient->corrected_nv_od = $request->c_nv_od;
        $patient->corrected_nv_os = $request->c_nv_os;
        $patient->optical = $request->optical;
        $patient->colorvision = $request->colorvision;
        $patient->as = $request->as;
        $patient->ad = $request->ad;
        $patient->save();
        ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Created a vital sign information for patient '.$id]);
        return redirect()->route('patients')->with('status','Successfully added a vital sign to patient '.$id);
    }   

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vitalsign  $vitalsign
     * @return \Illuminate\Http\Response
     */
    public function show(Vitalsign $vitalsign)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vitalsign  $vitalsign
     * @return \Illuminate\Http\Response
     */
    public function edit(Vitalsign $vitalsign,$id)
    {
        $vitalsign = Vitalsign::where('information_id',$id)->first();
        return view('vitalsign.edit')->with('vitals',$vitalsign);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vitalsign  $vitalsign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vitalsign $vitalsign,$id)
    {
        $request->validate([
            'height' => 'required',
            'weight' => 'required',
            'bp'     => 'required',
            'pulse' => 'required',
            'respiration' => 'required',
            'bmi' => 'required',
            'ppd' => 'required',
            'mmr' => 'required',
            'bodybuilt' => 'required',
            'lmp' => 'nullable',
            'lsc' => 'nullable',
            'method' => 'nullable',
            'uc_fv_od' => 'required|integer',
            'uc_fv_os' => 'required|integer',
            'c_fv_od' => 'required|integer',
            'c_fv_os' => 'required|integer',
            'uc_nv_od' => 'required|integer',
            'uc_nv_os' => 'required|integer',
            'c_nv_od' => 'required|integer',
            'c_nv_os' => 'required|integer',
            'optical' => 'required|boolean',
            'colorvision' => 'required|boolean',
            'ad' => 'required|integer',
            'as' => 'required|integer'
        ]);
        $vitalsign = Vitalsign::find($id);
        $vitalsign->height = $request->height;
        $vitalsign->weight = $request->weight;
        $vitalsign->bp = $request->bp;
        $vitalsign->pulse = $request->pulse;
        $vitalsign->respiration = $request->respiration;
        $vitalsign->bmi = $request->bmi;
        $vitalsign->ppd = $request->ppd;
        $vitalsign->lmp = $request->lmp;
        $vitalsign->lsc = $request->lsc;
        $vitalsign->bodybuilt = $request->bodybuilt;
        $vitalsign->mmr = $request->mmr;
        $vitalsign->method = $request->method;
        $vitalsign->uncorrected_fv_od = $request->uc_fv_od;
        $vitalsign->unccorected_fv_os = $request->uc_fv_os;
        $vitalsign->uncorrected_nv_od = $request->uc_nv_od;
        $vitalsign->unccorected_nv_os = $request->uc_nv_os;
        $vitalsign->corrected_fv_od = $request->c_fv_od;
        $vitalsign->corrected_fv_os = $request->c_fv_os;
        $vitalsign->corrected_nv_od = $request->c_nv_od;
        $vitalsign->corrected_nv_os = $request->c_nv_os;
        $vitalsign->optical = $request->optical;
        $vitalsign->colorvision = $request->colorvision;
        $vitalsign->as = $request->as;
        $vitalsign->ad = $request->ad;
        $vitalsign->save();
        ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Updated a vital sign information for patient '.$id]);
        return redirect()->route('patients')->with('status','Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vitalsign  $vitalsign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vitalsign $vitalsign)
    {
        //
    }
}
