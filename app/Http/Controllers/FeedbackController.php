<?php

namespace App\Http\Controllers;

use App\Models\LabReport;
use Illuminate\Http\Request;
use Auth;
use App\Models\ActivityLog;
class FeedbackController extends Controller
{
    public function create($id)
    {
    	return view('remarks.create')->with('id',$id);
    }
    public function store($id,Request $request)
    {
    	$labRep = LabReport::where('information_id',$id)->first();
    	$labRep->psychotest = $request->psychtest;
    	$labRep->remarks = $request->remarks;
    	$labRep->suggestion = $request->suggestion;
    	$labRep->save();
    	ActivityLog::create(['user_id' => Auth::user()->id , 'action' => 'Created a remarks,suggestion and psychotest for patient '.$id]);
    	return redirect()->route('patients')->with('status','Remarks and suggestion added');
    }
}
