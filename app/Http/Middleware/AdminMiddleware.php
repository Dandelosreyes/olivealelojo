<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check() || Auth::user()->role_id != 4)
        {
            return redirect()->route('home')
                            ->with('error','You don\'t have the authority to access that specific page.');
        }
        return $next($request);
    }
}
